/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */

window.mobileCheck = function() {
    let check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
};

let homeSlider = function () {
    if ($('.home-blog__slider').length){
        const swiper = new Swiper('.home-blog__slider', {
            // If we need pagination
            slidesPerView: 'auto',
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.slide-next',
                prevEl: '.slide-prev',
            },

            // And if we need scrollbar
            scrollbar: {
                el: '.swiper-scrollbar',
            },

            breakpoints: {
                // when window width is >= 320px
                768: {
                    slidesPerView: 3,
                },
            }
        })
    }
}

const projectSlider = function (e){

    if ($('.projects__slider').length) {
        const swiper = new Swiper('.projects__slider', {
            // If we need pagination
            slidesPerView: 'auto',
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.slide-next',
                prevEl: '.slide-prev',
            },

            // And if we need scrollbar
            scrollbar: {
                el: '.swiper-scrollbar',
            },
        })
    }
}


let headerSearch = function () {
    const header_search = $('.header-search');
    let check_seacch_active = false;

    $(document).on('click', '.site-actions__search', function (e) {
        e.preventDefault();
        header_search.addClass('is-active');
        check_seacch_active = true;
    });

    $(document).keyup(function (e) {
        if (e.keyCode === 27 && check_seacch_active === true) {
            header_search.removeClass('is-active');
            check_seacch_active === false;
        }
    });
}

let detailMenuSticky = function () {
    const header_menu = $('.site-header');
    const detail_menu = $('.detail-menu__wrapper');
    if (detail_menu.length) {
        let detail_menu_top = detail_menu.offset().top;

        let scroll = $(window).scrollTop();
        if (scroll > detail_menu_top - header_menu.height()) {
            detail_menu.addClass('is-sticky');
            header_menu.css({
                'transform' : 'translateY(-100%)'
            })
        }

        $(window).scroll(function (e) {
            scroll = $(window).scrollTop();
            if (scroll > detail_menu_top - header_menu.height()) {
                header_menu.css({
                    'transform' : 'translateY(-100%)'
                })
                if (scroll > detail_menu_top){
                    detail_menu.addClass('is-sticky');
                }
            } else {
                detail_menu.removeClass('is-sticky');
                detail_menu.removeAttr('style');
                header_menu.removeAttr('style');
            }
        })
    }
}

const dataContentHover = function () {
    $(document).on('mousemove', '[data-content-hover]', function (e) {
        const id = $(this).attr('data-content-hover');
        let content = $('[data-content-active="' + id + '"]');
        $(this).addClass('is-active');
        $(this).siblings().removeClass('is-active');
        content.addClass('is-active');
        content.siblings().removeClass('is-active');
    })
}

const detailMenuActiveOnScroll = function () {
    let detail_menu = $('.detail-menu');
    let detail_menu_array = []

    if (detail_menu.length){
        detail_menu.find('ul li a').each(function (e) {
            let menu_item = $(this);
            let id = menu_item.attr('href');
            if ($(id).offset()) {
              let offset = $(id).offset().top - 110 - 80 - 1;
              detail_menu_array.push({
                id: id,
                top: offset
              });
            }
        });

        let scrollFunction = function (e){
            detail_menu_array.map((v, index) => {
                if (scroll > v.top && detail_menu_array[index + 1] != undefined && scroll < detail_menu_array[index + 1].top) {
                    $('[href="' + v.id + '"]').parent().addClass('is-active');
                } else {
                    if (detail_menu_array[index + 1] === undefined && scroll > v.top){
                        $('[href="' + v.id + '"]').parent().addClass('is-active');
                    }
                    else{
                        $('[href="' + v.id + '"]').parent().removeClass('is-active');
                    }
                }
            });
        }

        let scroll = $(window).scrollTop();
        scrollFunction();


        $(window).scroll(function (e) {
            scroll = $(window).scrollTop();
            scrollFunction();
        });

        $(document).on('click','.detail-menu ul li a',function(e) {
            e.preventDefault();
            let id = $(this).attr('href');
            $('html, body').animate({
                scrollTop: $(id).offset().top - 190
            }, 500);
        });
    }
}

const productDetailSlider = function (){

    if ($('.product-detail__slider').length) {
        const swiper = new Swiper('.product-detail__slider', {
            // If we need pagination
            slidesPerView: 'auto',
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            // And if we need scrollbar
            scrollbar: {
                el: '.swiper-scrollbar',
            },
        })
    }
}

const tabsActive = function (e){
    $(document).on('click','[data-tab-active]',function (e){
        e.preventDefault();
        let $this = $(this);
        let content = $this.siblings('[data-tab-content]');
        if (!$this.parent().hasClass('is-active')){
            $('[data-tab-active]').parent().removeClass('is-active');
            $('[data-tab-content]').slideUp();
            content.slideDown();
            $this.parent().addClass('is-active');
        }
        else{
            $('[data-tab-active]').parent().removeClass('is-active');
            $('[data-tab-content]').slideUp();
        }
    })
}

const menuToggle = function (e){
    $(document).on('click','.site-actions__toggle',function (e){
        const $this = $(this);
        $('.site-header').toggleClass('is-toggle');
    })

    $(document).on('click','.menu-item-has-children a',function (e){
        const $this = $(this);
        if (!$this.parent().hasClass('is-active')){
            $('.menu-item-has-children').removeClass('is-active');
            $('.menu-item-has-children').children('.sub-menu').slideUp();
            $this.parent().addClass('is-active');
            $this.siblings('.sub-menu').slideDown();
        }
        else{
            $('.menu-item-has-children').removeClass('is-active');
            $('.menu-item-has-children').children('.sub-menu').slideUp();
        }
    })
}

const footerMenuToggle = function (){
    $(document).on('click','.footer-menu__heading',function (e){
        $(this).parent().siblings().children('.footer-menu__heading').removeClass('is-active');
        $(this).toggleClass('is-active');
        $(this).parent().siblings().children('ul').slideUp();
        $(this).siblings('ul').slideToggle();
    })
}

const shopFilterPopup = function (){
    $(document).on('click','.filter-button',function (e){
        $('.filter-popup').addClass('is-active');
        $('body').addClass('has-popup');
    })

    $(document).on('click',function (e){
        if(!$(e.target).hasClass('filter-button') && !$(e.target).parents().hasClass('is-active')){
            $('.filter-popup').removeClass('is-active');
            $('body').removeClass('has-popup');
        }
    })

    $(document).on('click','.filter-popup__close',function (e){
        $('.filter-popup').removeClass('is-active');
        $('body').removeClass('has-popup');
    })
}

const setInputValue = function(arr, value) {
  arr.map(function() {
    if(this.value === value) {
      this.checked = true;
    }
  });
}

const filterOnMobile = function() {
  let categoryVal = '';
  let attributeVal = '';
  let orderByVal = '';
  let paramArray = new Array();
  let s = '';

  $('.js-popup-category').change(function(){
    if ( $(this).is(":checked") ) {
      const categoryVal = $(this).val();
      paramArray['product_cat'] = 'product_cat=' + categoryVal;
    }
  })

  $('.js-popup-attribute').change(function(){
    if ( $(this).is(":checked") ) {
      const attributeVal = $(this).val();
      paramArray['filter_size'] = 'filter_size=' + attributeVal;
    }
  })

  $('.js-popup-orderby').change(function(){
    if ( $(this).is(":checked") ) {
      const orderByVal = $(this).val();
      paramArray['orderby'] = 'orderby=' + orderByVal;
    }
  })

  $('.js-apply-filter').click(function(e) {
    e.preventDefault();
    for (var i in paramArray) {
      s += paramArray[i] + "&";
    }
    window.location.search = '?' + s;
  })

  $('.js-filter-clear').click(function(e) {
    e.preventDefault();
    $('.js-popup-orderby').prop('checked', false);
    $('.js-popup-attribute').prop('checked', false);
    $('.js-popup-category').prop('checked', false);
    paramArray = [];
  })

  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const productParam = urlParams.get('product_cat') ? urlParams.get('product_cat') : '';
  const sizeParam = urlParams.get('filter_size') ? urlParams.get('filter_size') : '';
  const orderByParam = urlParams.get('orderby') ? urlParams.get('orderby') : '';

  const popUpProductCat = $('.js-popup-category');
  if (popUpProductCat.length) {
    setInputValue(popUpProductCat, productParam);
    paramArray['product_cat'] = 'product_cat=' + productParam;
  }

  const popUpSize = $('.js-popup-attribute');
  if (popUpSize.length) {
    setInputValue(popUpSize, sizeParam);
    paramArray['filter_size'] = 'filter_size=' + sizeParam;
  }

  const popUpOrderBy = $('.js-popup-orderby');
  if (popUpOrderBy.length) {
    setInputValue(popUpOrderBy, orderByParam);
    paramArray['orderby'] = 'orderby=' + orderByParam;
  }
}

$(".js-go-to-overview").click(function(e) {
  e.preventDefault();
  $('html, body').animate({
    scrollTop: $("#overview").offset().top
  }, 500);
});

const openSubMenu = function() {
  const menuItems = $('.menu-toggle__nav .menu-item');
  menuItems.map(function() {
    $(this).click(function(e) {
      e.stopPropagation();
      if (e.target === this) {
        $(this).toggleClass('is-active').siblings().removeClass('is-active');
      }
    });
  });
}

let init = function() {
    headerSearch();
    homeSlider();
    if (!window.mobileCheck()){
        detailMenuSticky();
    }
    dataContentHover();
    detailMenuActiveOnScroll();
    projectSlider();
    productDetailSlider();
    tabsActive();
    menuToggle();
    footerMenuToggle();
    shopFilterPopup();
    filterOnMobile();
    openSubMenu();
}

jQuery(document).ready(function ($) {
    init();
});
