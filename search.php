<?php
get_header();

global $wp_query;
$search_count = $wp_query->found_posts;
?>
<div class="search">
  <div class="container container--search">
    <div class="search__inner">
      <?php if ( $search_count > 0) : ?>
        <div class="h3 search__result__meta">
          <?php echo __('Search Results in Products:', 'sport-floor'); ?>
          <?php the_search_query(); ?>
        </div>
        <div class="page-list search__result__list">
          <div class="f page-list__wrapper">
            <div class="page-list__inner">
              <div class="grid grid--three-columns grid--doubling justify--center search-posts__grid">
                <?php while ( have_posts() ) { the_post();
                  if ( $post->post_type == 'product') :
                    $search_post_id = $post->ID;
                    $search_post_title = get_the_title($post->ID);
                    $search_post_link = get_the_permalink($post->ID);
                    $search_post_image = get_the_post_thumbnail_url($post->ID, 'full');
                    $search_post_date = get_the_date('F j, Y', $post->ID);
                    ?>
                    <article class="grid__column<?php if( !empty($class) ) : echo ' ' . $class; endif; ?>"">
                    <div class="grid__column__inner">
                      <a href="<?= $search_post_link; ?>">
                        <?php if ( !empty($search_post_image) ) : ?>
                          <div class="search-card__image-wrapper">
                            <div class="search-card__image">
                              <img src="<?php echo $search_post_image; ?>" alt="">
                            </div>
                          </div>
                        <?php endif; ?>
                      </a>
                      <div class="search-card__body">
                        <span class="blog-grid__date"><?= $search_post_date; ?></span>
                        <?php if ( !empty($search_post_title) ) : ?>
                          <h4 class="search-card__title"><a href="<?= $search_post_link; ?>"><?= $search_post_title; ?></a></h4>
                        <?php endif; ?>
                      </div>
                    </div>
                    </article>
                  <?php endif; }; ?>
              </div>
            </div>
          </div>
        </div>
        <div class="h3 search__result__meta">
          <?php echo __('Search Results in Blog:', 'sport-floor'); ?>
          <?php the_search_query(); ?>
        </div>
        <div class="page-list search__result__list">
          <div class="f page-list__wrapper">
            <div class="page-list__inner">
              <div class="grid grid--three-columns grid--doubling justify--center search-posts__grid">
                <?php while ( have_posts() ) { the_post();
                  if ( $post->post_type == 'post') :
                    $search_post_id = $post->ID;
                    $search_post_title = get_the_title($post->ID);
                    $search_post_link = get_the_permalink($post->ID);
                    $search_post_image = get_the_post_thumbnail_url($post->ID, 'full');
                    $search_post_date = get_the_date('F j, Y', $post->ID);
                    ?>
                    <article class="grid__column<?php if( !empty($class) ) : echo ' ' . $class; endif; ?>"">
                      <div class="grid__column__inner">
                        <a href="<?= $search_post_link; ?>">
                          <?php if ( !empty($search_post_image) ) : ?>
                            <div class="search-card__image-wrapper">
                              <div class="search-card__image">
                                <img src="<?php echo $search_post_image; ?>" alt="">
                              </div>
                            </div>
                          <?php endif; ?>
                        </a>
                        <div class="search-card__body">
                          <span class="blog-grid__date"><?= $search_post_date; ?></span>
                          <?php if ( !empty($search_post_title) ) : ?>
                            <h4 class="search-card__title"><a href="<?= $search_post_link; ?>"><?= $search_post_title; ?></a></h4>
                          <?php endif; ?>
                        </div>
                      </div>
                    </article>
                  <?php endif; }; ?>
              </div>
            </div>
          </div>
        </div>
        <footer class="search-results__footer">
          <?php $big = 999999999;
          $total_page = $wp_query->max_num_pages;
          if ( !empty( $total_page ) || $total_page > 1 ) :
            $paged = empty( $paged ) ? max( 1, get_query_var('paged') ) : $paged;
            $args = array(
              'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
              'format' => '?paged=%#%',
              'current' => $paged,
              'total' => $total_page,
              'prev_text' => '<span class="pagination--prev">' . __('Previous', 'sport-floor') . '</span>',
              'next_text' => '<span class="pagination--next">' . __('Next', 'sport-floor') . '</span>',
            );
            ?>
            <div class="pagination search-results__pagination">
              <div class="flex align--center pagination__list">
                <?php echo paginate_links($args); ?>
              </div>
            </div>
          <?php endif; ?>
        </footer>
      <?php else : ?>
        <div class="search__result--empty">
          <h4 class="h4 search__result--empty-title">
            <?php echo _e('No Results for:', 'sport-floor'); ?>
            <?php the_search_query(); ?>
          </h4>
        </div>
      <?php endif; ?>
    </div>
  </div>
</div>
<?php get_footer(); ?>
