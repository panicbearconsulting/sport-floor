<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Sport_floor
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function sport_floor_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'sport_floor_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function sport_floor_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'sport_floor_pingback_header' );

/**
 * Add support to upload SVG icons
 */
function cc_mime_types( $mimes ){
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );

/**
 * Returns page id by template file name
 *
 * @param string $template name of template file including .php
 */
function sport_floor_get_page_id($template_name) {
  $args = [
    'post_type' => 'page',
    'fields' => 'ids',
    'nopaging' => true,
    'meta_key' => '_wp_page_template',
    'meta_value' => $template_name . '.php',
    'hierarchical' => 0,
    'posts_per_page' => 1
  ];
  $pages = get_posts( $args );
  foreach ( $pages as $page ) {
    $url = get_permalink($page);
  }

  return $url;
}
