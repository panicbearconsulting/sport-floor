<?php

add_filter( 'rwmb_meta_boxes', 'sport_floor_register_meta_boxes' );

function sport_floor_register_meta_boxes( $meta_boxes ) {
    $prefix = 'sport_floor';

    // Contact page settings
    $meta_boxes[] = array (
        'title' => esc_html__( 'Contact information', 'sport_floor' ),
        'id' => 'home',
        'post_types' => array(
            0 => 'page',
        ),
        'context' => 'normal',
        'priority' => 'high',
        'autosave' => true,
        'include' => array(
            'template' => 'templates/contact.php'
        ),
        'fields' => array(
            array (
                'id' => $prefix . 'addresses',
                'type' => 'group',
                'name' => esc_html__( 'Addresses', 'sport_floor' ),
                'fields' => array(
                    array (
                        'id' => $prefix . 'location',
                        'type' => 'text',
                        'name' => esc_html__( 'Location name', 'sport_floor' ),
                    ),
                    array (
                        'id' => $prefix . 'address',
                        'type' => 'textarea',
                        'name' => esc_html__( 'Address', 'sport_floor' ),
                    ),
                ),
                'clone' => 1,
                'sort_clone' => 1,
                'default_state' => 'expanded',
                'collapsible' => true,
                'group_title' => '{sport_floorlocation}',
                'save_state' => true,
                'max_clone' => 4,
            ),
        ),
        'text_domain' => 'sport_floor',
    );

    // Homepage settings
    $meta_boxes[] = array (
        'title' => esc_html__( 'Home Banner', 'sport_floor' ),
        'id' => 'home',
        'post_types' => array(
            0 => 'page',
        ),
        'context' => 'normal',
        'priority' => 'high',
        'autosave' => true,
        'include' => array(
            'template' => 'templates/homepage.php'
        ),
        'fields' => array(
            array (
                'id' => $prefix . 'home_title',
                'type' => 'textarea',
                'name' => esc_html__( 'Heading', 'sport_floor' ),
                'std' => 'Ideal Place for your Ideal Sport Floor',
            ),
            array (
                'id' => $prefix . 'home_description',
                'type' => 'textarea',
                'name' => esc_html__( 'Description', 'sport_floor' ),
                'std' => 'We offer the best sport floors to be suitable with your purpose and to ensure the smoothness of your games.',
            ),
            array (
                'id' => $prefix . 'home_link',
                'type' => 'text',
                'name' => esc_html__( 'Button link', 'sport_floor' ),
                'std' => '#',
            ),
            array (
                'id' => $prefix . 'home_banner',
                'type' => 'single_image',
                'name' => esc_html__( 'Home banner', 'sport_floor' ),
            ),
        ),
        'text_domain' => 'sport_floor',
        'show' => array(
            'relation' => 'AND',
            'template' => array(
                0 => 'page:templates/homepage.php',
            ),
        ),
    );

    $meta_boxes[] = array (
        'title' => esc_html__( 'Product category', 'sport_floor' ),
        'id' => 'product_category',
        'post_types' => array(
            0 => 'page',
        ),
        'context' => 'normal',
        'priority' => 'high',
        'autosave' => true,
        'include' => array(
            'template' => 'templates/homepage.php'
        ),
        'fields' => array(
            array (
                'id' => $prefix . 'product_category_heading',
                'type' => 'text',
                'name' => esc_html__( 'Title', 'sport_floor' ),
            ),
            array (
                'id' => $prefix . 'product_category_page',
                'type' => 'post',
                'name' => esc_html__( 'Type product select', 'online-generator' ),
                'post_type'  => 'page',
                'field_type' => 'select_advanced',
                'clone'      => true,
            ),
        ),
        'text_domain' => 'sport_floor',
        'show' => array(
            'relation' => 'AND',
            'template' => array(
                0 => 'page:templates/homepage.php',
            ),
        ),
    );

    $meta_boxes[] = array (
        'title' => esc_html__( 'MAINTENANCE AND CARE', 'sport_floor' ),
        'id' => 'mac',
        'post_types' => array(
            0 => 'page',
        ),
        'context' => 'normal',
        'priority' => 'high',
        'autosave' => true,
        'include' => array(
            'template' => 'templates/homepage.php'
        ),
        'fields' => array(
            array (
                'id' => $prefix . 'mac_title',
                'type' => 'text',
                'name' => esc_html__( 'Title', 'sport_floor' ),
                'std' => 'MAINTENANCE AND CARE'
            ),
            array (
                'id' => $prefix . 'mac_heading',
                'type' => 'textarea',
                'name' => esc_html__( 'Heading', 'sport_floor' ),
                'std' => 'It is very important to care for your athletic floor.'
            ),
            array (
                'id' => $prefix . 'mac_list',
                'type' => 'group',
                'fields' => array(
                    array (
                        'id' => $prefix . 'title',
                        'type' => 'text',
                        'name' => esc_html__( 'Title', 'sport_floor' ),
                    ),
                    array (
                        'id' => $prefix . 'content',
                        'name' => esc_html__( 'Content', 'sport_floor' ),
                        'type' => 'textarea',
                    ),
                    array (
                        'id' => $prefix . 'link',
                      'type' => 'post',
                      'name' => esc_html__( 'URL', 'text-domain' ),
                      'post_type' => array(
                        0 => 'page',
                      ),
                      'field_type' => 'select_advanced',
                    ),
                ),
                'clone' => 1,
                'sort_clone' => 1,
                'default_state' => 'collapsed',
                'collapsible' => true,
                'group_title' => '{sport_floor_title}',
                'save_state' => true,
            ),
            array (
                'id' => $prefix . 'mac_banner',
                'type' => 'single_image',
                'name' => esc_html__( 'Banner', 'sport_floor' ),
            ),
        ),
        'text_domain' => 'sport_floor',
        'show' => array(
            'relation' => 'AND',
            'template' => array(
                0 => 'page:templates/homepage.php',
            ),
        ),
    );

    $meta_boxes[] = array (
        'title' => esc_html__( 'WHY US?', 'sport_floor' ),
        'id' => 'why-us',
        'post_types' => array(
            0 => 'page',
        ),
        'context' => 'normal',
        'priority' => 'high',
        'autosave' => true,
        'include' => array(
            'template' => 'templates/homepage.php'
        ),
        'fields' => array(
            array (
                'id' => $prefix . 'why_title',
                'type' => 'textarea',
                'name' => esc_html__( 'Title', 'sport_floor' ),
                'std' => 'WHY US?',
            ),
            array (
                'id' => $prefix . 'why_heading',
                'type' => 'textarea',
                'name' => esc_html__( 'Heading', 'sport_floor' ),
                'std' => 'We are one of the leading companies in sports facilities surfaces and equipment industry.',
            ),
            array (
                'id' => $prefix . 'why_banner',
                'type' => 'single_image',
                'name' => esc_html__( 'Banner', 'sport_floor' ),
            ),
            array (
                'id' => $prefix . 'why_list',
                'type' => 'group',
                'name' => esc_html__( 'Content', 'sport_floor' ),
                'fields' => array(
                    array (
                        'id' => $prefix . 'icon',
                        'type' => 'textarea',
                        'name' => esc_html__( 'icon', 'sport_floor' ),
                    ),
                    array (
                        'id' => $prefix . 'title',
                        'type' => 'text',
                        'name' => esc_html__( 'Title', 'sport_floor' ),
                    ),
                    array (
                        'id' => $prefix . 'description',
                        'type' => 'textarea',
                        'name' => esc_html__( 'Content', 'sport_floor' ),
                    ),
                ),
                'clone' => 1,
                'sort_clone' => 1,
                'default_state' => 'collapsed',
                'collapsible' => true,
                'save_state' => true,
                'group_title' => '{sport_floor_title}',
                'max_clone' => 3,
            ),
        ),
        'text_domain' => 'sport_floor',
        'show' => array(
            'relation' => 'AND',
            'template' => array(
                0 => 'page:templates/homepage.php',
            ),
        ),
    );


    $meta_boxes[] = array (
        'title' => esc_html__( 'BLOG', 'sport_floor' ),
        'id' => 'blog',
        'post_types' => array(
            0 => 'page',
        ),
        'context' => 'normal',
        'priority' => 'high',
        'autosave' => true,
        'include' => array(
            'template' => 'templates/homepage.php'
        ),
        'fields' => array(
            array (
                'id' => $prefix . 'blog_title',
                'type' => 'textarea',
                'name' => esc_html__( 'Title', 'sport_floor' ),
                'std' => 'BLOG',
            ),
            array (
                'id' => $prefix . 'blog_heading',
                'type' => 'textarea',
                'name' => esc_html__( 'Heading', 'sport_floor' ),
                'std' => 'Latest information & tips to take care of your sport floor.',
            ),
        ),
        'text_domain' => 'sport_floor',
        'show' => array(
            'relation' => 'AND',
            'template' => array(
                0 => 'page:templates/homepage.php',
            ),
        ),
    );


    $meta_boxes[] = array (
        'title' => esc_html__( 'Description', 'sport_floor' ),
        'id' => 'description',
        'post_types' => array(
            0 => 'page',
        ),
        'context' => 'normal',
        'priority' => 'high',
        'autosave' => true,
        'include' => array(
            'template' => 'templates/homepage.php'
        ),
        'fields' => array(
            array (
                'id' => $prefix . 'description_heading',
                'type' => 'textarea',
                'name' => esc_html__( 'Heading', 'sport_floor' ),
                'std' => 'Latest information & tips to take care of your sport floor.',
            ),
        ),
        'text_domain' => 'sport_floor',
        'show' => array(
            'relation' => 'AND',
            'template' => array(
                0 => 'page:templates/homepage.php',
            ),
        ),
    );

    return $meta_boxes;
}


add_filter( 'mb_settings_pages', function ( $settings_pages ) {
  $settings_pages[] = array(
    'id'            => 'my-options',
    'menu_title'    => 'Footer',
    'option_name'   => 'my_options',
    'icon_url'      => 'dashicons-images-alt',
    'submenu_title' => 'Footer',
  );
  $settings_pages[] = array(
    'id'            => 'shop_page_options',
    'menu_title'    => 'Shop page',
    'option_name'   => 'shop_page_options',
    'icon_url'      => 'dashicons-images-alt',
    'submenu_title' => 'Shop Page',
  );
  return $settings_pages;
} );
