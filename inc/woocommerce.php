<?php
/**
 * WooCommerce Compatibility File
 *
 * @link https://woocommerce.com/
 *
 * @package Sport_floor
 */

/**
 * WooCommerce setup function.
 *
 * @link https://docs.woocommerce.com/document/third-party-custom-theme-compatibility/
 * @link https://github.com/woocommerce/woocommerce/wiki/Enabling-product-gallery-features-(zoom,-swipe,-lightbox)
 * @link https://github.com/woocommerce/woocommerce/wiki/Declaring-WooCommerce-support-in-themes
 *
 * @return void
 */
function sport_floor_woocommerce_setup() {
	add_theme_support(
		'woocommerce',
		array(
			'thumbnail_image_width' => 60,
			'single_image_width'    => 300,
			'product_grid'          => array(
				'default_rows'    => 3,
				'min_rows'        => 1,
				'default_columns' => 4,
				'min_columns'     => 1,
				'max_columns'     => 6,
			),
		)
	);
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'sport_floor_woocommerce_setup' );

/**
 * WooCommerce specific scripts & stylesheets.
 *
 * @return void
 */
function sport_floor_woocommerce_scripts() {
	wp_enqueue_style( 'sport-floor-woocommerce-style', get_template_directory_uri() . '/woocommerce.css', array(), _S_VERSION );

	$font_path   = WC()->plugin_url() . '/assets/fonts/';
	$inline_font = '@font-face {
			font-family: "star";
			src: url("' . $font_path . 'star.eot");
			src: url("' . $font_path . 'star.eot?#iefix") format("embedded-opentype"),
				url("' . $font_path . 'star.woff") format("woff"),
				url("' . $font_path . 'star.ttf") format("truetype"),
				url("' . $font_path . 'star.svg#star") format("svg");
			font-weight: normal;
			font-style: normal;
		}';

	wp_add_inline_style( 'sport-floor-woocommerce-style', $inline_font );

  wp_enqueue_style( 'select2');
  wp_enqueue_script( 'selectinit', get_stylesheet_directory_uri() . '/js/vendors/select2/select2.js', array( 'selectWoo' ), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'sport_floor_woocommerce_scripts' );

/**
 * Disable the default WooCommerce stylesheet.
 *
 * Removing the default WooCommerce stylesheet and enqueing your own will
 * protect you during WooCommerce core updates.
 *
 * @link https://docs.woocommerce.com/document/disable-the-default-stylesheet/
 */
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

/**
 * Add 'woocommerce-active' class to the body tag.
 *
 * @param  array $classes CSS classes applied to the body tag.
 * @return array $classes modified to include 'woocommerce-active' class.
 */
function sport_floor_woocommerce_active_body_class( $classes ) {
	$classes[] = 'woocommerce-active';

	return $classes;
}
add_filter( 'body_class', 'sport_floor_woocommerce_active_body_class' );

/**
 * Change number of related products output
 */
add_filter( 'woocommerce_output_related_products_args', 'sport_floor_related_products_args', 20 );
function sport_floor_related_products_args( $args ) {
  $args['posts_per_page'] = 4; // 4 related products
  $args['columns'] = 4; // arranged in 4 columns
  return $args;
}

/**
 * Remove the breadcrumbs
 */
add_action( 'init', 'sport_floor_remove_wc_breadcrumbs' );
function sport_floor_remove_wc_breadcrumbs() {
  remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
}

/**
 * Change the breadcrumb separator
 */
add_filter( 'woocommerce_breadcrumb_defaults', 'sport_floor_change_breadcrumb_delimiter' );
function sport_floor_change_breadcrumb_delimiter( $defaults ) {
  // Change the breadcrumb delimeter from '/' to ''
  $defaults['delimiter'] = '  ';
  return $defaults;
}

/**
 * Removes the "shop" title on the main shop page
 */
add_filter( 'woocommerce_show_page_title', '__return_false' );

/**
 * Removes the results count on the main shop page
 */
add_action( 'after_setup_theme', 'sport_floor_remove_product_result_count', 99 );
function sport_floor_remove_product_result_count() {
  remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_result_count', 20 );
  remove_action( 'woocommerce_after_shop_loop' , 'woocommerce_result_count', 20 );
}

/**
 * Remove default WooCommerce wrapper.
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

/**
 * Get all Product Categories.
 */
function sport_floor_get_all_categories() {
  $cat_args = array(
    'orderby'    => 'name',
    'order'      => 'asc',
    'hide_empty' => true,
  );

  $list = get_terms( 'product_cat', $cat_args );

  return $list;
}

/**
 * Get all term of Product Attribute.
 */
function sport_floor_get_all_terms($attr) {
  $attribute_taxonomies = wc_attribute_taxonomy_name( $attr );

  $terms = get_terms( $attribute_taxonomies, array( 'hide_empty' => '1' ) );

  return $terms;
}

/**
 * Custom Filter Box on Shop page.
 */
function sport_floor_filter_box() {
  echo '<div class="flex align--center justify--center filter-box__inner">';
          global $wp;
          $found = false;
          $any_label = 'Size';
          $attribute_taxonomies = wc_attribute_taxonomy_name( 'size' );
          $_chosen_attributes   = WC_Query::get_layered_nav_chosen_attributes();
          $taxonomy = 'pa_size';
          $taxonomy_filter_name = wc_attribute_taxonomy_slug( $taxonomy );
          $category_filter_name = 'cat';
          $terms = get_terms( $attribute_taxonomies, array( 'hide_empty' => '1' ) );
          $multiple = '';
          $query_type = 'and';
          $current_values = isset( $_chosen_attributes[ $taxonomy ]['terms'] ) ? $_chosen_attributes[ $taxonomy ]['terms'] : array();

          if ( ! empty( $_GET ) ) {
            if (array_key_exists('product_cat', $_GET)) {
              $current_category[] = $_GET['product_cat'];
            }
          }

          if ( '' === get_option( 'permalink_structure' ) ) {
            $form_action = remove_query_arg( array( 'page', 'paged' ), add_query_arg( $wp->query_string, '', home_url( $wp->request ) ) );
          } else {
            $form_action = preg_replace( '%\/page/[0-9]+%', '', home_url( trailingslashit( $wp->request ) ) );
          }

          $category_label = 'All';
          $cat_args = array(
            'orderby'    => 'name',
            'order'      => 'asc',
            'hide_empty' => true,
          );

          $product_categories = get_terms( 'product_cat', $cat_args );

          // Category Form
          echo '<form method="get" action="' . esc_url( $form_action ) . '" class="woocommerce-category-filter">';
  echo '<select class="woocommerce-widget-layered-nav-dropdown dropdown_layered_nav_' . esc_attr( $category_filter_name ) . '"' . ( $multiple ? 'multiple="multiple"' : '' ) . '>';
          echo '<option value="">' . esc_html( $category_label ) . '</option>';

          if ( !empty($product_categories) ) {
            foreach ( $product_categories as $product_category ) {
              $option_is_set = in_array( $product_category->slug, $current_category, true );

              echo '<option value="' . esc_attr( urldecode( $product_category->slug ) ) . '" ' . selected( $option_is_set, true, false ) . '>' . esc_html( $product_category->name ) . '</option>';
            }
          }
          echo '</select>';
          echo '<input type="hidden" name="product_cat" value="' . esc_attr( $current_category ) . '" />';
          echo wc_query_string_form_fields( null, array( 'product_cat'), '', true );
          echo '</form>';

          // Attribute Form
          echo '<form method="get" action="' . esc_url( $form_action ) . '" class="woocommerce-widget-layered-nav-dropdown">';
          echo '<select class="woocommerce-widget-layered-nav-dropdown dropdown_layered_nav_' . esc_attr( $taxonomy_filter_name ) . '"' . ( $multiple ? 'multiple="multiple"' : '' ) . '>';
          echo '<option value="">' . esc_html( $any_label ) . '</option>';

          foreach ( $terms as $term ) {

            // If on a term page, skip that term in widget list.
            if ( $term->term_id === 0 ) {
              continue;
            }

            // Get count based on current view.
            $option_is_set = in_array( $term->slug, $current_values, true );

            echo '<option value="' . esc_attr( urldecode( $term->slug ) ) . '" ' . selected( $option_is_set, true, false ) . '>' . esc_html( $term->name ) . '</option>';
          }

          echo '</select>';

          if ( $multiple ) {
            echo '<button class="woocommerce-widget-layered-nav-dropdown__submit" type="submit" value="' . esc_attr__( 'Apply', 'woocommerce' ) . '">' . esc_html__( 'Apply', 'woocommerce' ) . '</button>';
          }

          if ( 'or' === $query_type ) {
            echo '<input type="hidden" name="query_type_' . esc_attr( $taxonomy_filter_name ) . '" value="or" />';
          }

          echo '<input type="hidden" name="filter_' . esc_attr( $taxonomy_filter_name ) . '" value="' . esc_attr( implode( ',', $current_values ) ) . '" />';
          echo wc_query_string_form_fields( null, array( 'filter_' . $taxonomy_filter_name, 'query_type_' . $taxonomy_filter_name ), '', true ); // @codingStandardsIgnoreLine
          echo '</form>';
          woocommerce_catalog_ordering();

          wc_enqueue_js(
        "
              // Update value on change.
              jQuery( '.dropdown_layered_nav_" . esc_js( $taxonomy_filter_name ) . "' ).change( function() {
                var slug = jQuery( this ).val();
                jQuery( ':input[name=\"filter_" . esc_js( $taxonomy_filter_name ) . "\"]' ).val( slug );
      
                // Submit form on change if standard dropdown.
                if ( ! jQuery( this ).attr( 'multiple' ) ) {
                  jQuery( this ).closest( 'form' ).submit();
                }
              });
              
              jQuery( '.dropdown_layered_nav_" . esc_js( $category_filter_name ) . "' ).change( function() {
                var slug2 = jQuery( this ).val();
                console.log(jQuery( this ));
                jQuery( ':input[name=\"product_" . esc_js( $category_filter_name ) . "\"]' ).val( slug2 );
      
                // Submit form on change if standard dropdown.
                if ( ! jQuery( this ).attr( 'multiple' ) ) {
                  jQuery( this ).closest( 'form' ).submit();
                }
              });
      
              // Use Select2 enhancement if possible
              if ( jQuery().selectWoo ) {
                var wc_layered_nav_select = function() {
                  jQuery( '.dropdown_layered_nav_" . esc_js( $taxonomy_filter_name ) . "' ).selectWoo( {
                    minimumResultsForSearch: 5,
                    width: '100%',
                    allowClear: " . 'false' . ",
                    language: {
                      noResults: function() {
                        return '" . esc_js( _x( 'No matches found', 'enhanced select', 'woocommerce' ) ) . "';
                      }
                    }
                  } );
                };
                wc_layered_nav_select();
                
                var wc_filter_category_select = function() {
                  jQuery( '.dropdown_layered_nav_" . esc_js( $category_filter_name ) . "' ).selectWoo( {
                    minimumResultsForSearch: 5,
                    width: '100%',
                    allowClear: " . 'false' . ",
                    language: {
                      noResults: function() {
                        return '" . esc_js( _x( 'No matches found', 'enhanced select', 'woocommerce' ) ) . "';
                      }
                    }
                  } );
                };
                wc_filter_category_select();
              }
            "
          );
          return $found;
        echo '</div>';
}

if ( ! function_exists( 'sport_floor_woocommerce_wrapper_before' ) ) {
	/**
	 * Before Content.
	 *
	 * Wraps all WooCommerce content in wrappers which match the theme markup.
	 *
	 * @return void
	 */
	function sport_floor_woocommerce_wrapper_before() {
		?>
      <?php
        // Footer Setting
        $shop_hero_title = rwmb_meta( 'shop_hero_title', ['object_type' => 'setting'], 'shop_page_options' );
        $shop_hero_description = rwmb_meta( 'shop_hero_description', ['object_type' => 'setting'], 'shop_page_options' );
        $shop_hero_image = rwmb_meta( 'shop_hero_image', ['object_type' => 'setting'], 'shop_page_options' );
      ?>
			<main id="primary" class="site-main">
        <?php if ( !empty($shop_hero_title) or !empty($shop_hero_description) or !empty($shop_hero_image) ) : ?>
          <section class="hero-banner page-banner">
            <?php if ( !empty($shop_hero_image) ) : ?>
              <div class="hero-banner__image">
                <img src="<?php echo $shop_hero_image['full_url']; ?>" alt="Shop page banner">
              </div>
            <?php endif; ?>
            <?php if ( !empty($shop_hero_title) or !empty($shop_hero_description) ) : ?>
              <div class="hero-banner__content pdt--40">
                <div class="container shop-banner__content-container">
                  <?php if ( !empty($shop_hero_title) ) : ?>
                    <h1 class="text--center"><?= $shop_hero_title; ?></h1>
                  <?php endif; ?>
                  <?php if ( !empty($shop_hero_description) ) : ?>
                    <p class="text--center text--white"><?= $shop_hero_description; ?></p>
                  <?php endif; ?>
                </div>
              </div>
            <?php endif; ?>
          </section>
        <?php endif; ?>
        <section class="filter filter-box filter-box--mobile">
          <div class="container desk--hidden tablet--hidden large--hidden">
            <a href="#" class="btn btn--outline filter-button one-whole"><?php esc_html_e('Sort & Filter', 'sport-floor'); ?></a>
          </div>
          <div class="filter-popup">
            <div class="filter-popup__header">
              <h5 class="filter-popup__title">
                <?php esc_html_e('Sort & Filter', 'sport-floor'); ?>
              </h5>
              <div class="filter-popup__action">
                <a href="#" class="filter-popup__clear js-filter-clear"><?php esc_html_e('Clear All', 'sport-floor'); ?></a>
                <a href="#" class="filter-popup__close">
                </a>
              </div>
            </div>
            <form action="" method="GET">
              <div class="filter-popup__select">
                <span class="filter-popup__label"><?php esc_html_e('Product', 'sport-floor'); ?></span>
                <?php
                $product_categories = sport_floor_get_all_categories();
                if ( !empty($product_categories) ) {
                  foreach ($product_categories as $product_category ) : ?>
                    <label class="filter-popup__radio">
                      <input type="radio" name="product" class="js-popup-category" value="<?= $product_category->slug; ?>">
                      <span class="checkmark"></span>
                      <span class="label"><?= $product_category->name; ?></span>
                    </label>
                <?php endforeach; } ?>
              </div>
              <div class="filter-popup__select">
                <span class="filter-popup__label"><?php esc_html_e('Size', 'sport-floor'); ?></span>
                <?php
                $terms = sport_floor_get_all_terms('size');
                if( !empty($terms) ) {
                  foreach ( $terms as $term ) : ?>
                  <label class="filter-popup__radio">
                    <input type="radio" name="size" class="js-popup-attribute" value="<?= $term->slug; ?>">
                    <span class="checkmark"></span>
                    <span class="label"><?= $term->name; ?></span>
                  </label>
                <?php endforeach; } ?>
              </div>
              <div class="filter-popup__select">
                <span class="filter-popup__label"><?php esc_html_e('Product', 'sport-floor'); ?></span>
                <label class="filter-popup__radio">
                  <input type="radio" name="sort-by" class="js-popup-orderby" value="date">
                  <span class="checkmark"></span>
                  <span class="label"><?php esc_html_e('Newest', 'sport-floor'); ?></span>
                </label>
                <label class="filter-popup__radio">
                  <input type="radio" name="sort-by" class="js-popup-orderby" value="price-desc">
                  <span class="checkmark"></span>
                  <span class="label"><?php esc_html_e('Price (High - Low)', 'sport-floor'); ?></span>
                </label>
                <label class="filter-popup__radio">
                  <input type="radio" name="sort-by" class="js-popup-orderby" value="price">
                  <span class="checkmark"></span>
                  <span class="label"><?php esc_html_e('Price (Low - High)', 'sport-floor'); ?></span>
                </label>
                <label class="filter-popup__radio">
                  <input type="radio" name="sort-by" class="js-popup-orderby" value="popularity">
                  <span class="checkmark"></span>
                  <span class="label"><?php esc_html_e('Most Popular', 'sport-floor'); ?></span>
                </label>
              </div>
              <a href="#" class="btn btn--primary filter-popup__apply js-apply-filter"><?php esc_html_e('Apply Filter', 'sport-floor'); ?></a>
            </form>
          </div>
        </section>
        <section class="filter-box filter-box--desktop">
          <div class="container filter-box__container">
            <?php sport_floor_filter_box(); ?>
          </div>
        </section>
        <div class="container shop-page__container">
		<?php
	}
}
//add_action( 'woocommerce_before_main_content', 'woocommerce_catalog_ordering', 20 );
add_action( 'woocommerce_before_main_content', 'sport_floor_woocommerce_wrapper_before' );
/**
 * Remove default WooCommerce sorting on Shop page.
 */
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

if ( ! function_exists( 'sport_floor_woocommerce_wrapper_after' ) ) {
	/**
	 * After Content.
	 *
	 * Closes the wrapping divs.
	 *
	 * @return void
	 */
	function sport_floor_woocommerce_wrapper_after() {
		?>
          <?php echo sport_floor_pagination(); ?>
        </div>
      </main><!-- #main -->
		<?php
	}
}
add_action( 'woocommerce_after_main_content', 'sport_floor_woocommerce_wrapper_after' );

/**
 * Sample implementation of the WooCommerce Mini Cart.
 *
 * You can add the WooCommerce Mini Cart to header.php like so ...
 *
	<?php
		if ( function_exists( 'sport_floor_woocommerce_header_cart' ) ) {
			sport_floor_woocommerce_header_cart();
		}
	?>
 */

// Wrap product thumbnail
add_action( 'woocommerce_before_shop_loop_item', 'sport_floor_start_product_thumb_wrap', 5 );
add_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_link_close', 5 );
add_action( 'woocommerce_shop_loop_item_title', 'sport_floor_start_action_button', 6 );
add_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 7 );
add_action( 'woocommerce_shop_loop_item_title', 'sport_floor_end_action_button', 8 );
add_action( 'woocommerce_shop_loop_item_title', 'sport_floor_end_product_thumb_wrap', 9 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );

/**
 * Custom product thumbnail wrap
 */
function sport_floor_start_product_thumb_wrap() {
	do_action( 'sport_floor_start_product_thumb_wrap' );
	?>
	<div class="sport-floor-product-thumb">
	<?php
}

/**
 * Custom product action button
 */
function sport_floor_start_action_button() {
	global $product;
	do_action( 'sport_floor_start_action_button' );
	?>
	<div class="sport-floor-action-button">
	<?php
}

/**
 * Shop end action button
 */
function sport_floor_end_action_button() {
	?>
	</div>
	<?php
	do_action( 'sport_floor_end_action_button' );
}

/**
 * Shop end product thumbnail wrap
 */
function sport_floor_end_product_thumb_wrap() {
  ?>
  </div>
  <?php
  do_action( 'sport_floor_end_product_thumb_wrap' );
}

/**
 *  Set product title as link to product page
 **/
// define the woocommerce_shop_loop_item_title callback
remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10, 2 );

if ( ! function_exists( 'codeless_woocommerce_template_loop_product_title' ) ) {
  /**
   * Show the product title in the product loop. By default this is an H2.
   */
  function sport_floor_woocommerce_template_loop_product_title() {
    global $product;

    $link = apply_filters( 'woocommerce_template_loop_product_title', get_the_permalink(), $product );



    echo '<a href="' . esc_url( $link ) . '"><h2 class="' . esc_attr( apply_filters( 'woocommerce_product_loop_title_classes', 'woocommerce-loop-product__title' ) ) . '">' . get_the_title() . '</h2></a>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
  }
};

// add the action
add_action( 'woocommerce_shop_loop_item_title', 'sport_floor_woocommerce_template_loop_product_title', 10, 2 );

if ( ! function_exists( 'sport_floor_woocommerce_cart_link_fragment' ) ) {
	/**
	 * Cart Fragments.
	 *
	 * Ensure cart contents update when products are added to the cart via AJAX.
	 *
	 * @param array $fragments Fragments to refresh via AJAX.
	 * @return array Fragments to refresh via AJAX.
	 */
	function sport_floor_woocommerce_cart_link_fragment( $fragments ) {
		ob_start();
		sport_floor_woocommerce_cart_link();
		$fragments['a.cart-contents'] = ob_get_clean();

		return $fragments;
	}
}
add_filter( 'woocommerce_add_to_cart_fragments', 'sport_floor_woocommerce_cart_link_fragment' );

if ( ! function_exists( 'sport_floor_woocommerce_cart_link' ) ) {
	/**
	 * Cart Link.
	 *
	 * Displayed a link to the cart including the number of items present and the cart total.
	 *
	 * @return void
	 */
	function sport_floor_woocommerce_cart_link() {
		?>
		<a class="cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'sport-floor' ); ?>">
			<?php
			$item_count_text = sprintf(
				/* translators: number of items in the mini cart. */
				_n( '%d item', '%d items', WC()->cart->get_cart_contents_count(), 'sport-floor' ),
				WC()->cart->get_cart_contents_count()
			);
			?>
			<span class="amount"><?php echo wp_kses_data( WC()->cart->get_cart_subtotal() ); ?></span> <span class="count"><?php echo esc_html( $item_count_text ); ?></span>
		</a>
		<?php
	}
}

if ( ! function_exists( 'sport_floor_woocommerce_header_cart' ) ) {
	/**
	 * Display Header Cart.
	 *
	 * @return void
	 */
	function sport_floor_woocommerce_header_cart() {
		if ( is_cart() ) {
			$class = 'current-menu-item';
		} else {
			$class = '';
		}
		?>
		<ul id="site-header-cart" class="site-header-cart">
			<li class="<?php echo esc_attr( $class ); ?>">
				<?php sport_floor_woocommerce_cart_link(); ?>
			</li>
			<li>
				<?php
				$instance = array(
					'title' => '',
				);

				the_widget( 'WC_Widget_Cart', $instance );
				?>
			</li>
		</ul>
		<?php
	}
}

// Minimum CSS to remove +/- default buttons on input field type number
add_action( 'wp_head' , 'custom_quantity_fields_css' );
function custom_quantity_fields_css(){
  ?>
  <style>
    .quantity input::-webkit-outer-spin-button,
    .quantity input::-webkit-inner-spin-button {
      display: none;
      margin: 0;
    }
    .quantity input.qty {
      appearance: textfield;
      -webkit-appearance: none;
      -moz-appearance: textfield;
    }
  </style>
  <?php
}


add_action( 'wp_footer' , 'custom_quantity_fields_script' );
function custom_quantity_fields_script(){
  ?>
  <script type='text/javascript'>
    jQuery( function( $ ) {
      if ( ! String.prototype.getDecimals ) {
        String.prototype.getDecimals = function() {
          var num = this,
            match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
          if ( ! match ) {
            return 0;
          }
          return Math.max( 0, ( match[1] ? match[1].length : 0 ) - ( match[2] ? +match[2] : 0 ) );
        }
      }
      // Quantity "plus" and "minus" buttons
      $( document.body ).on( 'click', '.plus, .minus', function() {
        var $qty        = $( this ).closest( '.quantity' ).find( '.qty'),
          currentVal  = parseFloat( $qty.val() ),
          max         = parseFloat( $qty.attr( 'max' ) ),
          min         = parseFloat( $qty.attr( 'min' ) ),
          step        = $qty.attr( 'step' );

        // Format values
        if ( ! currentVal || currentVal === '' || currentVal === 'NaN' ) currentVal = 0;
        if ( max === '' || max === 'NaN' ) max = '';
        if ( min === '' || min === 'NaN' ) min = 0;
        if ( step === 'any' || step === '' || step === undefined || parseFloat( step ) === 'NaN' ) step = 1;

        // Change the value
        if ( $( this ).is( '.plus' ) ) {
          if ( max && ( currentVal >= max ) ) {
            $qty.val( max );
          } else {
            $qty.val( ( currentVal + parseFloat( step )).toFixed( step.getDecimals() ) );
          }
        } else {
          if ( min && ( currentVal <= min ) ) {
            $qty.val( min );
          } else if ( currentVal > 0 ) {
            $qty.val( ( currentVal - parseFloat( step )).toFixed( step.getDecimals() ) );
          }
        }

        // Trigger change event
        $qty.trigger( 'change' );
      });
    });
  </script>
  <?php
}

add_shortcode ('woo_cart_but', 'woo_cart_but' );
/**
 * Create Shortcode for WooCommerce Cart Menu Item
 */
function woo_cart_but() {
  ob_start();

  $cart_count = WC()->cart->cart_contents_count; // Set variable for cart item count
  $cart_url = wc_get_cart_url();  // Set Cart URL
//  woocommerce_mini_cart();

  ?>
  <div class="site-actions__mini-cart">
    <div class="mini-cart">
      <div class="widget_shopping_cart_content"><?php echo woocommerce_mini_cart(); ?></div>
    </div>
    <a href="<?php echo $cart_url; ?>" class="site-actions__cart">
      <svg width="16" height="16" viewBox="0 0 16 16" fill="none"><g><path d="M15.7754 4.5087a.6672.6672 0 00-.3469-.7726.6667.6667 0 00-.2951-.0694H3.698a.166.166 0 01-.1646-.1334L3.152 1.72A2.176 2.176 0 001.034 0a.8333.8333 0 100 1.6667.5013.5013 0 01.4887.4l2.1106 10.046c.1095.515.4026.9726.8247 1.2873a.166.166 0 01.0087.2593 1.3333 1.3333 0 102.0667.4154.1655.1655 0 01.0681-.2197.168.168 0 01.0812-.021h3.98a.1666.1666 0 01.1493.2407A1.3335 1.3335 0 0012.0054 16a1.3335 1.3335 0 001.3314-1.2767 1.3328 1.3328 0 00-.1415-.6566.1612.1612 0 01.0064-.1568.1613.1613 0 01.137-.0766.8334.8334 0 100-1.6666H5.754a.5022.5022 0 01-.4893-.3967l-.1194-.57a.1665.1665 0 01.1634-.2h7.1693a2.0003 2.0003 0 001.9294-1.474l1.368-5.0173z" fill="currentColor"/></g><defs><clipPath><path fill="currentColor" d="M0 0h16v16H0z"/></clipPath></defs></svg>
      <span class="cart-contents" title="My Basket">
      <?php
      if ( $cart_count > 0 ) {
        ?>
        <span class="cart-contents-count"><?php echo $cart_count; ?></span>
        <?php
      }
      ?>
    </span>
    </a>
  </div>

  <?php

  return ob_get_clean();

}

add_filter( 'woocommerce_add_to_cart_fragments', 'woo_cart_but_count' );
/**
 * Add AJAX Shortcode when cart contents update
 */
function woo_cart_but_count( $fragments ) {

  ob_start();

  $cart_count = WC()->cart->cart_contents_count;
  $cart_url = wc_get_cart_url();

  ?>
  <span class="cart-contents" title="<?php _e( 'View your shopping cart' ); ?>">
    <?php
    if ( $cart_count > 0 ) {
      ?>
      <span class="cart-contents-count"><?php echo $cart_count; ?></span>
      <?php
    }
    ?>
  </span>
  <?php

  $fragments['span.cart-contents'] = ob_get_clean();

  return $fragments;
}

/**
 * Change number of products that are displayed per page (shop page)
 */

add_filter( 'loop_shop_per_page', 'sport_floor_loop_shop_per_page', 20 );
function sport_floor_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = 12;

  return $cols;
}

remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );
function sport_floor_pagination() {

  global $wp_query;

  $big = 999999999; // need an unlikely integer

  $pages = paginate_links( array(
    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
    'format' => '?paged=%#%',
    'current' => max( 1, get_query_var('paged') ),
    'total' => $wp_query->max_num_pages,
    'type'  => 'array',
    'prev_text'    => __('Previous', 'sport-floor'),
    'next_text'    => __('Next', 'sport-floor'),
  ) );
  if( is_array( $pages ) ) {
    $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
    echo '<div class="pagination-wrap"><ul class="pagination">';
    foreach ( $pages as $page ) {
      echo "<li>$page</li>";
    }
    echo '</ul></div>';
  }
}
