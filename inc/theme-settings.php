<?php
// Register a theme options page
add_filter( 'mb_settings_pages', function ( $settings_pages ) {
    $settings_pages[] = array(
        'id'          => 'theme-settings',
        'option_name' => 'theme-settings',
        'menu_title'  => 'Theme Settings',
        'icon_url'    => 'dashicons-edit',
        'style'       => 'boxes',
        'columns'     => 1,
        'position' => 2,
        'tabs'        => array(
            'general' => 'General Settings',
            'form' => 'Form Settings',
            'addresses' => 'Address Settings',
        ),
    );
    return $settings_pages;
} );

// Register meta boxes and fields for settings page
add_filter( 'rwmb_meta_boxes', function ( $meta_boxes ) {
    $meta_boxes[] = array(
        'id'             => 'general',
        'title'          => 'General',
        'settings_pages' => 'theme-settings',
        'tab'            => 'general',
        'fields' => array(
            array(
                'id'               => 'logo',
                'name'             => 'Logo',
                'type'             => 'single_image'
            ),
            array(
                'id'               => 'light_logo',
                'name'             => 'Light Logo',
                'type'             => 'single_image'
            ),
            array(
                'type' => 'divider',
            ),
            array(
                'id'               => 'favicon',
                'name'             => 'Favicon',
                'type'             => 'single_image'
            ),
            array(
                'type' => 'divider',
            ),
            array(
                'id'      => 'hotline',
                'name'    => 'Hotline',
                'type'    => 'text',
                'clone' => true
            ),
            array(
                'type' => 'divider',
            ),
            array(
                'id'      => 'email',
                'name'    => 'Email',
                'type'    => 'text',
                'clone' => true
            ),

        ),
    );

    $meta_boxes[] = array(
        'id'             => 'forms',
        'title'          => 'Forms',
        'settings_pages' => 'theme-settings',
        'tab'            => 'form',
        'fields' => array(
            array (
                'id' => 'contact_form',
                'type' => 'fieldset_text',
                'name' => esc_html__( 'Contact form ID', 'nn' ),
                'rows' => 2,
                'options' => array(
                    'en_US' => 'English',
                    'vi' => 'Vietnamese',
                ),
            ),
            array (
                'id' => 'inquiry_form',
                'type' => 'fieldset_text',
                'name' => esc_html__( 'Inquiry form ID', 'nn' ),
                'rows' => 2,
                'options' => array(
                    'en_US' => 'English',
                    'vi' => 'Vietnamese',
                ),
            ),
            array (
                'id' => 'application_form',
                'type' => 'fieldset_text',
                'name' => esc_html__( 'CV application form ID', 'nn' ),
                'rows' => 2,
                'options' => array(
                    'en_US' => 'English',
                    'vi' => 'Vietnamese',
                ),
            ),
            array (
                'id' => 'ir_form',
                'type' => 'fieldset_text',
                'name' => esc_html__( 'Investor contact form ID', 'nn' ),
                'rows' => 2,
                'options' => array(
                    'en_US' => 'English',
                    'vi' => 'Vietnamese',
                ),
            ),

        ),
    );

    $meta_boxes[] = array (
        'title' => esc_html__( 'Addresses', 'nn' ),
        'id' => 'addresses',
        'settings_pages' => 'theme-settings',
        'tab'            => 'addresses',
        'fields' => array(
            array (
                'id' => 'head_office',
                'type' => 'group',
                'fields' => array(
                    array (
                        'id' => 'address',
                        'type' => 'group',
                        'name' => esc_html__( 'Address', 'nn' ),
                        'rows' => 2,
                        'fields' => array(
                            array(
                                'id' => 'en_US',
                                'type' => 'textarea',
                                'name' => 'English',
                            ),
                            array(
                                'id' => 'vi',
                                'type' => 'textarea',
                                'name' => 'Vietnamese',
                            ),
                        )
                    ),
                    array (
                        'id' => 'gmap_url',
                        'type' => 'url',
                        'name' => esc_html__( 'Google Map URL', 'nn' ),
                    ),
                ),
                'default_state' => 'expanded',
                'collapsible' => true,
                'group_title' => 'Head office',
            ),
            array (
                'id' => 'divider_6o5e1ix9fa3',
                'type' => 'divider',
                'name' => esc_html__( 'Divider', 'nn' ),
            ),
            array (
                'id' => 'factory_locations',
                'type' => 'group',
                'fields' => array(
//                    array (
//                        'id' => 'name',
//                        'type' => 'text',
//                        'name' => esc_html__( 'Factory name', 'nn' ),
//                    ),
                    array (
                        'id' => 'name',
                        'type' => 'fieldset_text',
                        'name' => esc_html__( 'Factory name', 'nn' ),
                        'rows' => 2,
                        'options' => array(
                            'en_US' => 'English',
                            'vi' => 'Vietnamese',
                        ),
                    ),
//                    array (
//                        'id' => 'address',
//                        'type' => 'fieldset_text',
//                        'name' => esc_html__( 'Factory address', 'nn' ),
//                        'rows' => 2,
//                        'options' => array(
//                            'en_US' => 'English',
//                            'vi' => 'Vietnamese',
//                        ),
//                    ),
                    array (
                        'id' => 'address',
                        'type' => 'group',
                        'name' => esc_html__( 'Factory address', 'nn' ),
                        'rows' => 2,
                        'fields' => array(
                            array(
                                'id' => 'en_US',
                                'type' => 'textarea',
                                'name' => 'English',
                            ),
                            array(
                                'id' => 'vi',
                                'type' => 'textarea',
                                'name' => 'Vietnamese',
                            ),
                        )
                    ),
                    array (
                        'id' => 'gmap_url',
                        'type' => 'url',
                        'name' => esc_html__( 'Factory Google Map URL', 'nn' ),
                    ),
                    array (
                        'id' => 'featured_image',
                        'type' => 'single_image',
                        'name' => esc_html__( 'Featured Image', 'nn' ),
                    ),
                ),
                'clone' => 1,
                'default_state' => 'expanded',
                'collapsible' => true,
                'group_title' => 'Factory locations',
                'save_state' => true,
            ),
        ),
        'text_domain' => 'nn',
    );

    return $meta_boxes;
} );