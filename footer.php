<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Sport_floor
 */

// Footer Setting
$footer_contact_address = rwmb_meta( 'footer_contact_address', ['object_type' => 'setting'], 'my_options' );
$footer_contact_phone = rwmb_meta( 'footer_contact_phone', ['object_type' => 'setting'], 'my_options' );
$footer_contact_fax = rwmb_meta( 'footer_contact_fax', ['object_type' => 'setting'], 'my_options' );

$footer_menus = rwmb_meta( 'footer_menus', ['object_type' => 'setting'], 'my_options' );

$footer_copyright = rwmb_meta( 'footer_copyright', ['object_type' => 'setting'], 'my_options' );
$footer_bottom_menu_list = rwmb_meta( 'footer_bottom_menu', ['object_type' => 'setting'], 'my_options' );
$short_code = rwmb_meta( 'newsletter_form_id', ['object_type' => 'setting'], 'my_options' );
$newsletter_form_title = rwmb_meta( 'newsletter_form_title', ['object_type' => 'setting'], 'my_options' );

?>

	<footer id="colophon" class="site-footer">
    <div class="container">
      <div class="footer-search">
          <div class="grid">
              <div class="grid__column four-twelfths mobile--one-whole">
                <?php if ( !empty($newsletter_form_title) ) : ?>
                  <h2 class="footer-search__heading"><?= $newsletter_form_title; ?></h2>
                <?php endif; ?>
                  <div class="tablet--hidden desk--hidden large--hidden">
                    <?php if ( !empty($footer_contact_address) ) : ?>
                      <p class="footer-info__location"><?= $footer_contact_address; ?></p>
                    <?php endif; ?>
                    <?php if ( !empty($footer_contact_phone) or !empty($footer_contact_fax) ) : ?>
                      <div class="footer-info__contact">
                        <?php if ( !empty($footer_contact_phone) ) : ?>
                          <a href="tel:">
                            Tel: <?= $footer_contact_phone; ?>
                          </a>
                        <?php endif; ?>
                        <?php if ( !empty($footer_contact_fax) ) : ?>
                          <a href="tel:">
                            Fax: <?= $footer_contact_fax; ?>
                          </a>
                        <?php endif; ?>
                      </div>
                    <?php endif; ?>
                  </div>
              </div>
              <div class="grid__column eight-twelfths mobile--one-whole">
                  <div class="footer-search__form">
                      <?php if ( !empty($short_code) ) { echo do_shortcode($short_code); } ?>
                  </div>
              </div>
          </div>
      </div>
      <div class="footer-info">
        <div class="grid">
          <div class="grid__column three-twelfths mobile--hidden">
            <?php if ( !empty($footer_contact_address) ) : ?>
              <p class="footer-info__location"><?= $footer_contact_address; ?></p>
            <?php endif; ?>
            <?php if ( !empty($footer_contact_phone) or !empty($footer_contact_fax) ) : ?>
              <div class="footer-info__contact">
                <?php if ( !empty($footer_contact_phone) ) : ?>
                  <a href="tel:">
                    Tel: <?= $footer_contact_phone; ?>
                  </a>
                <?php endif; ?>
                <?php if ( !empty($footer_contact_fax) ) : ?>
                  <a href="tel:">
                    Fax: <?= $footer_contact_fax; ?>
                  </a>
                <?php endif; ?>
              </div>
            <?php endif; ?>
          </div>
          <div class="grid__column nine-twelfths mobile--one-whole">
            <?php if ($footer_menus) : ?>
              <div class="footer-menu">
                <?php foreach ( $footer_menus as $menu ) : ?>
                  <div class="footer-menu__item">
                    <?php if ( !empty($menu['footer_menu_title'] ) ) : ?>
                      <span class="footer-menu__heading"><?= $menu['footer_menu_title']; ?></span>
                    <?php endif; ?>
                    <ul>
                      <?php
                      $args=array(
                        'post_type' => 'page',
                        'post__in' => $menu['footer_menu_list']
                      );

                      $my_query = new wp_query($args);
                      if( $my_query->have_posts() ) : ?>
                        <?php
                        while ($my_query->have_posts()) :
                          $my_query->the_post();
                          ?>
                          <li><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></li>
                        <?php endwhile; ?>
                      <?php
                      endif;
                      wp_reset_query();
                      ?>
                    </ul>
                  </div>
                <?php endforeach; ?>
              </div>
            <?php endif; ?>
          </div>
        </div>
        <div class="footer-copyright grid--aligned-center">
          <div class="grid">
            <?php if ( !empty($footer_copyright) ) : ?>
              <div class="grid__column four-twelfths mobile--one-whole">
                <span><?= $footer_copyright; ?></span>
              </div>
            <?php endif; ?>
            <?php if ( !empty($footer_bottom_menu_list) ) : ?>
              <div class="grid__column eight-twelfths mobile--one-whole">
                <div class="footer-copyright__menu">
                  <?php foreach ( $footer_bottom_menu_list as $item ) : ?>
                    <?php if ( !empty($item['url']) && !empty($item['title']) ) : ?>
                      <a href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a>
                    <?php endif; ?>
                  <?php endforeach; ?>
                </div>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
