<div class="product-card">
    <div class="product-card__thumbnail">
        <a href="#">
            <img src="<?php echo get_template_directory_uri() . '/images/project-1.jpg' ?>"
                 alt="Wood Floor Cleaner">
        </a>
        <a href="#" class="product-card__add">
            Add to Cart
        </a>
    </div>
    <h4 class="product-card__title">
        <a href="#">Wood Floor Cleaner</a>
    </h4>
    <span class="product-card__price">
        $ 68.00
    </span>
</div>