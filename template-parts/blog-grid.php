<div class="blog-grid">
    <div class="blog-grid__thumbnail">
        <a href="<?php echo get_the_permalink(); ?>">
            <?php the_post_thumbnail();?>
        </a>
    </div>
    <span class="blog-grid__date">
        <?php echo get_the_date(); ?>
    </span>
    <h3 class="blog-grid__title">
        <a href="<?php echo get_the_permalink(); ?>">
        <?php echo get_the_title(); ?>
        </a>
    </h3>
    <p class="blog-grid__description">
        <?php echo get_the_excerpt(); ?>
    </p>
    <a href="<?php echo get_the_permalink(); ?>" class="readmore">
        Read more
        <span>
            <svg width="14" height="14" viewBox="0 0 14 14" fill="none"><path d="M11.1362 3.0488v5.0154m0-5.0154H6.1209m5.0153 0L2.6441 11.541" stroke="currentColor" stroke-width="1.5"/></svg>
        </span>
    </a>
</div>