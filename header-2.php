<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Sport_floor
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Barlow:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">

	<header id="masthead" class="site-header site-header--white">
        <div class="container">
            <div class="site-header__wrapper">
                <div class="site-branding">
                    <a href="/">
                        <?php
                            if (the_custom_logo()) :
                                the_custom_logo();
                            else :
                                echo '<img src="' . get_template_directory_uri() . '/images/logo.svg" alt="' . get_bloginfo( 'name' ) .'">';
                            endif;
                        ?>
                    </a>
                </div><!-- .site-branding -->

                <nav id="site-navigation" class="main-navigation">
                    <?php
                    wp_nav_menu(
                        array(
                            'theme_location' => 'menu-1',
                            'menu_id'        => 'primary-menu',
                        )
                    );
                    ?>
                </nav><!-- #site-navigation -->
                <div class="site-actions">
                    <a href="#" class="site-actions__search">
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none"><circle cx="7" cy="7" r="6" stroke="currentColor" stroke-width="2"/><path d="M15 15l-4-4" stroke="currentColor" stroke-width="2"/></svg>
                    </a>
                  <?php echo do_shortcode("[woo_cart_but]"); ?>
                    <button class="site-actions__toggle">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                </div>
            </div>
            <div class="menu-toggle">
                <nav class="menu-toggle__nav">
                    <?php
                    wp_nav_menu(
                        array(
                            'theme_location' => 'menu-1',
                            'menu_id'        => 'primary-menu',
                        )
                    );
                    ?>
                </nav><!-- #site-navigation -->
                <div class="menu-toggle__info">
                    <p>Centaur Products Inc. 3145 Thunderbird Crescent
                        Burnaby BC V5A 3G1</p>
                    <div class="grid grid--two-columns">
                        <div class="grid__column">
                            <a href="#">
                                Tel: (604) 430-3088
                            </a>
                        </div>
                        <div class="grid__column">
                            <a href="#">
                                Fax: (604) 430-1393
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</header><!-- #masthead -->

    <div class="header-search">
        <div class="container">
            <div class="grid grid--aligned-center align--center">
                <div class="grid__column eight-twelfths">
                  <form role="search" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="GET">
                    <label class="header-search__label">
                      <input type="text" name="s" class="header-search__control" placeholder="<?php esc_attr_e( 'Search for products', 'sport-floor' ); ?>">
                    </label>
                    <p class="header-search__meta"><?php esc_html_e('Hit enter to search or ESC to close', 'sport-floor'); ?></p>
                    <button style="display: none"></button>
                  </form>
                </div>
            </div>
        </div>
    </div>
