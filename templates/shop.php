<?php
/**
 * Template Name: Shop
 *
 * @package Sport floor
 */

get_header();
?>
    <section class="hero-banner page-banner">
        <div class="hero-banner__image">
            <img src="<?php echo get_template_directory_uri() . '/images/shop-banner.jpg'; ?>" alt="Home banner">
        </div>
        <div class="hero-banner__content pdt--40">
            <div class="container">
                <h1 class="text--center">Gymnasium Equipment</h1>
                <p class="text--center text--white">We offer gymnasium equipments to fully serve your demands<br> that
                    can make your projects complete perfectly</p>
            </div>
        </div>
    </section>
    <div class="filter">
        <form class="filter-form mobile--hidden">
            <div class="filter-form__wrapper">
                <div class="form__field">
                    <div class="custom-select">
                        <select class="form__control">
                            <option value="Gym Wall Padding">Gym Wall Padding</option>
                            <option value="Gym Wall Padding">Indoor Basketball Hoop</option>
                            <option value="Gym Wall Padding">Basketball Backstops</option>
                            <option value="Gym Wall Padding">Volleyball Equipment</option>
                            <option value="Gym Wall Padding">Gym Bleachers</option>
                            <option value="Gym Wall Padding">Gym Floor Covers</option>
                        </select>
                    </div>
                </div>
                <div class="form__field">
                    <div class="custom-select">
                        <select class="form__control">
                            <option value="Size">Size</option>
                            <option value="Gym Wall Padding">50m x 50m</option>
                            <option value="Gym Wall Padding">100m x 100m</option>
                            <option value="Gym Wall Padding">150m x 150m</option>
                            <option value="Gym Wall Padding">200m x 200m</option>
                        </select>
                    </div>
                </div>
                <div class="form__field">
                    <div class="custom-select">
                        <select class="form__control">
                            <option value="Most Popular">Newest</option>
                            <option value="Gym Wall Padding">Price (High - Low)</option>
                            <option value="Gym Wall Padding">Price (Low - High)</option>
                            <option value="Gym Wall Padding">Most Popular</option>
                        </select>
                    </div>
                </div>
            </div>
        </form>
        <div class="container desk--hidden tablet--hidden large--hidden">
            <a href="#" class="btn btn--outline filter-button one-whole">
                Sort & Filter
            </a>
        </div>
    </div>
    <div class="filter-popup">
        <div class="filter-popup__header">
            <h5 class="filter-popup__title">
                Sort & Filter
            </h5>
            <div class="filter-popup__action">
                <a href="#" class="filter-popup__clear">Clear All</a>
                <a href="#" class="filter-popup__close">
                </a>
            </div>
        </div>
        <form action="" method="GET">
            <div class="filter-popup__select">
                    <span class="filter-popup__label">
                        PRODUCT
                    </span>
                <label class="filter-popup__radio">
                    <input type="radio" name="product" checked>
                    <span class="checkmark"></span>
                    <span class="label">
                            Gym Wall Padding
                        </span>
                </label>
                <label class="filter-popup__radio">
                    <input type="radio" name="product">
                    <span class="checkmark"></span>
                    <span class="label">
                            Indoor Basketball Hoop
                        </span>
                </label>
                <label class="filter-popup__radio">
                    <input type="radio" name="product">
                    <span class="checkmark"></span>
                    <span class="label">
                            Basketball Backstops
                        </span>
                </label>
                <label class="filter-popup__radio">
                    <input type="radio" name="product">
                    <span class="checkmark"></span>
                    <span class="label">
                            Volleyball Equipment
                        </span>
                </label>
                <label class="filter-popup__radio">
                    <input type="radio" name="product">
                    <span class="checkmark"></span>
                    <span class="label">
                            Gym Bleachers
                        </span>
                </label>
                <label class="filter-popup__radio">
                    <input type="radio" name="product">
                    <span class="checkmark"></span>
                    <span class="label">
                            Gym Floor Covers
                        </span>
                </label>
            </div>
            <div class="filter-popup__select">
                    <span class="filter-popup__label">
                        SIZE
                    </span>
                <label class="filter-popup__radio">
                    <input type="radio" name="size" checked>
                    <span class="checkmark"></span>
                    <span class="label">
                            50m x 50m
                        </span>
                </label>
                <label class="filter-popup__radio">
                    <input type="radio" name="size">
                    <span class="checkmark"></span>
                    <span class="label">
                            100m x 100m
                        </span>
                </label>
                <label class="filter-popup__radio">
                    <input type="radio" name="size">
                    <span class="checkmark"></span>
                    <span class="label">
                            150m x 150m
                        </span>
                </label>
                <label class="filter-popup__radio">
                    <input type="radio" name="size">
                    <span class="checkmark"></span>
                    <span class="label">
                            200m x 200m
                        </span>
                </label>
            </div>
            <div class="filter-popup__select">
                    <span class="filter-popup__label">
                        PRODUCT
                    </span>
                <label class="filter-popup__radio">
                    <input type="radio" name="sort-by" checked>
                    <span class="checkmark"></span>
                    <span class="label">
                            Newest
                        </span>
                </label>
                <label class="filter-popup__radio">
                    <input type="radio" name="sort-by">
                    <span class="checkmark"></span>
                    <span class="label">
                            Price (High - Low)
                        </span>
                </label>
                <label class="filter-popup__radio">
                    <input type="radio" name="sort-by">
                    <span class="checkmark"></span>
                    <span class="label">
                            Price (Low - High)
                        </span>
                </label>
                <label class="filter-popup__radio">
                    <input type="radio" name="sort-by">
                    <span class="checkmark"></span>
                    <span class="label">
                            Most Popular
                        </span>
                </label>
            </div>
        </form>
    </div>
    </div>
    <section class="section shop-product">
        <div class="container">
            <div class="grid grid--four-columns shop-grid">
                <?php for ($i = 1; $i <= 12; $i++) : ?>
                    <div class="grid__column">
                        <?php get_template_part("template-parts/products/product-card"); ?>
                    </div>
                <?php endfor; ?>
            </div>
            <div class="pagination">
                <a href="#" class="pagination__button pagination__disabled">Previous</a>
                <span class="pagination__number is-active">1</span>
                <a href="#" class="pagination__number">2</a>
                <a href="#" class="pagination__number">3</a>
                <span class="pagination__number">...</span>
                <a href="#" class="pagination__number">12</a>
                <a href="#" class="pagination__button">Next</a>
            </div>
        </div>
    </section>
<?php
get_footer();
