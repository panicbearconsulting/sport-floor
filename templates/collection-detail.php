<?php
/**
 * Template Name: Collections Detail
 *
 * @package Sport floor
 */

get_header();

$current_page = get_permalink();

// Collection Detail Setting
$sub_title = rwmb_meta( 'collection_detail_sub_title' );
$title = rwmb_meta( 'collection_detail_title' );
$description = rwmb_meta( 'collection_detail_description' );
$hero_image = rwmb_meta( 'collection_detail_hero_image' );
$nav_items = rwmb_meta( 'collection_detail_nav_items' );
$nav_text_mobile = rwmb_meta( 'collection_detail_nav_text_mobile' );
$nav_btn_text = rwmb_meta( 'anchor_bar_btn_text' );
$nav_btn_url = rwmb_meta( 'anchor_bar_btn_url' );
$about_heading = rwmb_meta( 'collection_detail_about_heading' );
$about_image = rwmb_meta( 'collection_detail_about_image' );
$about_description = rwmb_meta( 'collection_detail_about_description' );
$about_anchor_id = rwmb_meta( 'about_anchor_id' );
$features_anchor_id = rwmb_meta( 'features_anchor_id' );
$features_title = rwmb_meta( 'collection_detail_features_title' );
$features_list = rwmb_meta( 'collection_detail_features_list' );
$benefits_anchor_id = rwmb_meta( 'benefits_anchor_id' );
$benefits_title = rwmb_meta( 'benefits_heading' );
$benefits_list = rwmb_meta( 'benefits_list' );
$products_anchor_id = rwmb_meta( 'products_anchor_id' );
$products_title = rwmb_meta( 'collection_detail_products_heading' );
$products_list = rwmb_meta( 'collection_detail_products_list' );
$projects_title = rwmb_meta( 'projects_heading' );
$projects_list = rwmb_meta( 'projects_list' );
$contact_heading = rwmb_meta( 'collection_detail_contact_heading' );
$contact_btn_text = rwmb_meta( 'collection_detail_contact_btn_text' );
$contact_btn_url = rwmb_meta( 'collection_detail_contact_btn_url' );
$contact_image = rwmb_meta( 'collection_detail_contact_image' );

?>
<section class="hero-banner hero-banner--center">
    <?php if ( !empty($hero_image) ) : ?>
      <div class="hero-banner__image">
        <img src="<?php echo $hero_image['full_url']; ?>" alt="Collection Detail banner">
      </div>
    <?php endif; ?>
    <div class="hero-banner__content pdt--40">
      <div class="container">
        <?php if ( !empty($sub_title) ) : ?>
          <span class="sub-heading"><?= $sub_title; ?></span>
        <?php endif; ?>
        <?php if ( !empty($title) ) : ?>
          <h1><?= $title; ?></h1>
        <?php endif; ?>
        <?php if ( !empty($description) ) : ?>
          <p class="text--white"><?= $description; ?></p>
        <?php endif; ?>
        <a href="#" class="btn btn--outline btn--white js-go-to-overview"><?php esc_html_e('Learn More', 'sport-floor'); ?></a>
      </div>
    </div>
  </section>
<?php if ( !empty($nav_items) ) : ?>
  <div class="detail-menu detail-menu--collection">
    <div class="detail-menu__wrapper">
      <?php if ( !empty($nav_text_mobile) ) : ?>
        <h5 class="text--white text--bold no-style tablet--hidden desk--hidden large--hidden"><?= $nav_text_mobile; ?></h5>
      <?php endif; ?>
      <?php if ( !empty($nav_items) ) : ?>
        <ul class="no-style mobile--hidden">
          <?php foreach ( $nav_items as $item ) : ?>
            <li><a href="<?= '#' . $item['nav_link']; ?>"><?= $item['nav_text']; ?></a></li>
          <?php endforeach; ?>
        </ul>
      <?php endif; ?>
      <?php if ( !empty($nav_btn_text) && !empty($nav_btn_url) ) : ?>
        <a href="<?= get_permalink($nav_btn_url); ?>" class="btn"><?= $nav_btn_text; ?></a>
      <?php endif; ?>
    </div>
  </div>
<?php endif; ?>
  <section class="section detail-about" id="<?php if ( !empty($about_anchor_id) ) : echo $about_anchor_id; endif; ?>">
    <div class="container">
      <?php if ( !empty($about_heading) ) : ?>
        <div class="heading tablet--hidden large--hidden desk--hidden">
          <h2 class="heading__title text--center"><?= $about_heading; ?></h2>
        </div>
      <?php endif; ?>
      <div class="grid grid--aligned-center mgb--50">
        <?php if ( !empty($about_image) ) : ?>
          <div class="grid__column six-twelfths mobile--one-whole">
            <img src="<?php echo $about_image['full_url']; ?>" alt="">
          </div>
        <?php endif; ?>
        <div class="grid__column one-twelfth mobile--hidden"></div>
        <?php if ( !empty($about_description) ) : ?>
          <div class="grid__column five-twelfths mobile--one-whole">
            <?php if ( !empty($about_heading) ) : ?>
              <div class="heading mobile--hidden">
                <h2 class="heading__title"><?= $about_heading; ?></h2>
              </div>
            <?php endif; ?>
            <p><?= $about_description ;?></p>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </section>
  <section class="section section--dark detail-features" id="<?php if ( !empty($features_anchor_id) ) : echo $features_anchor_id; endif; ?>">
    <?php if ( !empty($features_list) ) : ?>
      <div class="detail-features__thumbnail">
        <?php foreach ( $features_list as $key => $item ) : ?>
          <?php $imgs = $item['features_image']; ?>
          <div class="detail-features__image <?php if ( $key == 0 ) : echo 'is-active'; endif; ?>" data-content-active="<?= $key; ?>">
            <?php echo '<img src="' . wp_get_attachment_image_url( $item['features_image'], 'full_url' ) . '" alt="">'; ?>
          </div>
        <?php endforeach; ?>
      </div>
      <div class="container">
        <div class="grid grid--aligned-middleright">
          <div class="grid__column four-twelfths mobile--one-whole">
            <?php if ( !empty($features_title) ) : ?>
              <div class="heading heading--white">
                <h2><?= $features_title; ?></h2>
              </div>
            <?php endif; ?>
            <div class="detail-features__list">
              <?php foreach ( $features_list as $key => $item ) : ?>
                <div class="detail-features__item <?php if ( $key == 0 ) : echo 'is-active'; endif; ?>" data-content-hover="<?= $key; ?>">
                  <span class="detail-features__number">0<?= $key+1; ?></span>
                  <?php if ( !empty($item['feature_title']) ) : ?>
                    <h4 class="detail-features__title"><?= $item['feature_title']; ?></h4>
                  <?php endif; ?>
                  <?php if ( !empty($item['feature_description']) ) : ?>
                    <p><?= $item['feature_description']; ?></p>
                  <?php endif; ?>
                  <?php echo '<img src="' . wp_get_attachment_image_url( $item['features_image'], 'full_url' ) . '" alt="">'; ?>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>
  </section>
  <section class="section benefits" id="<?php if ( !empty($benefits_anchor_id) ) : echo $benefits_anchor_id; endif; ?>">
    <div class="container">
      <?php if ( !empty($benefits_title) ) : ?>
        <div class="heading">
          <h2 class="heading__title"><?= $benefits_title; ?></h2>
        </div>
      <?php endif; ?>

      <div class="grid grid--three-columns grid--doubling">
        <?php if ( !empty($benefits_list) ) : ?>
          <?php foreach ( $benefits_list as $item ) : ?>
            <div class="grid__column">
              <div class="benefits-card">
                <?php if ( !empty($item['benefits_image']) ) : ?>
                  <div class="benefits-card__icon">
                    <?php echo '<img src="' . wp_get_attachment_image_url( $item['benefits_image'], 'full_url' ) . '" alt="">'; ?>
                  </div>
                <?php endif; ?>
                <?php if ( !empty($item['benefit_title']) ) : ?>
                  <h4 class="benefits-card__title"><?= $item['benefit_title']; ?></h4>
                <?php endif; ?>
                <?php if ( !empty($item['benefit_description']) ) : ?>
                  <div class="benefits-card__content">
                    <p><?= $item['benefit_description']; ?></p>
                  </div>
                <?php endif; ?>
              </div>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>
      </div>
    </div>
  </section>
  <section class="section section--gray detail-products" id="<?php if ( !empty($products_anchor_id) ) : echo $products_anchor_id; endif; ?>">
    <div class="container">
      <?php if ( !empty($products_title) ) : ?>
        <div class="heading">
          <h2 class="heading__title text--center"><?= $products_title; ?></h2>
        </div>
      <?php endif; ?>
      <?php if (!empty($products_list) && count($products_list) > 0) : ?>
        <div class="grid grid--four-columns grid--aligned-center grid--doubling grid-collection__products">
          <?php
          $args=array(
            'post_type' => 'product',
            'post__in' => $products_list
          );

          $my_query = new wp_query($args);
          if( $my_query->have_posts() ) : ?>
            <?php
            while ($my_query->have_posts()) :
              $my_query->the_post();
              ?>
              <div class="grid__column">
                <div class="product-card">
                  <?php
                  $product = wc_get_product( get_the_ID() );
                  $sku = $product->get_sku();
                  ?>
                  <div class="product-card__thumbnail">
                    <a href="<?php echo get_the_permalink(); ?>">
                      <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php echo get_the_title(); ?>">
                    </a>
                    <a href="<?= '?add-to-cart=' . get_the_ID(); ?>" data-quantity="1" class="product-card__add button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="<?= get_the_ID(); ?>" data-product_sku="<?= $sku; ?>" aria-label="Add “<?= get_the_title(); ?>” to your cart" rel="nofollow">Add to cart</a>
                  </div>
                  <h4 class="product-card__title">
                    <a href="<?php echo get_the_permalink(); ?>">
                      <?php echo get_the_title(); ?>
                    </a>
                  </h4>
                </div>
              </div>
            <?php endwhile; ?>
          <?php
          endif;
          wp_reset_query();
          ?>
        </div>
      <?php endif; ?>
    </div>
  </section>
  <section class="section home-blog detail-blog">
    <div class="container">
      <?php if ( !empty($projects_title) ) : ?>
        <div class="heading nine-twelfths">
          <h2 class="heading__title"><?= $projects_title; ?></h2>
        </div>
      <?php endif; ?>
      <?php if ( !empty($projects_list) ) : ?>
        <div class="projects__slider swiper-container">
        <div class="swiper-wrapper">
          <?php foreach ( $projects_list as $item ) : ?>
          <div class="swiper-slide grid__column">
            <div class="project-grid">
              <?php if ( !empty($item['image']) ) : ?>
                <div class="project-grid__thumbnail">
                  <?php echo '<img src="' . wp_get_attachment_image_url( $item['image'], 'full_url' ) . '" alt="">'; ?>
                </div>
              <?php endif; ?>
              <?php if ( !empty($item['title']) ) : ?>
                <h3 class="project-grid__title"><?= $item['title']; ?></h3>
              <?php endif; ?>
              <?php if ( !empty($item['location']) ) : ?>
                <p class="project-grid__location"><?= $item['location']; ?></p>
              <?php endif; ?>
            </div>
          </div>
          <?php endforeach; ?>
        </div>
      </div>
      <?php endif; ?>
      <div class="home-blog__action">
        <div class="slide-arrow">
          <button class="slide-prev">
            <svg width="19" height="19" viewBox="0 0 19 19" fill="none">
              <path d="M1.9206 9.879l4.5597 4.5595M1.9207 9.8789l4.5596-4.5596M1.9207 9.8789h15.441"
                    stroke="currentColor" stroke-width="1.5"/>
            </svg>
          </button>
          <button class="slide-next">
            <svg width="19" height="19" viewBox="0 0 19 19" fill="none">
              <path d="M17.0792 9.8794l-4.5596 4.5596m4.5596-4.5596l-4.5596-4.5597m4.5596 4.5597H1.6382"
                    stroke="currentColor" stroke-width="1.5"/>
            </svg>
          </button>
        </div>
      </div>
    </div>
  </section>
<?php if ( !empty($contact_heading) or (!empty($contact_btn_text) and !empty($contact_btn_url)) or !empty($contact_image) ) : ?>
  <section class="section detail-contact-us mobile--hidden" id="contact-us">
    <div class="container">
      <div class="grid grid--aligned-center">
        <?php if ( !empty($contact_heading) or (!empty($contact_btn_text) and !empty($contact_btn_url)) ) : ?>
          <div class="grid__column five-twelfths">
            <?php if ( !empty($contact_heading) ) : ?>
              <div class="heading">
                <h2 class="heading__title"><?= $contact_heading; ?></h2>
              </div>
            <?php endif; ?>
            <?php if ( !empty($contact_btn_text) and !empty($contact_btn_url) ) : ?>
              <a href="<?= get_permalink($contact_btn_url); ?>" class="btn"><?= $contact_btn_text; ?></a>
            <?php endif; ?>
          </div>
        <?php endif; ?>
        <?php if ( !empty($contact_image) ) : ?>
          <div class="grid__column seven-twelfths">
            <img src="<?php echo $contact_image['full_url']; ?>" alt="">
          </div>
        <?php endif; ?>
      </div>
    </div>
  </section>
<?php endif; ?>
  <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<?php
get_footer();
