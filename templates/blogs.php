<?php
/**
 * Template Name: Blogs
 *
 * @package Sport floor
 */

$posts_list = rwmb_meta( 'featured_posts' );

echo get_template_part('header-2');
?>
<?php if (!empty($posts_list) && count($posts_list) > 0) : ?>
  <section class="blogs-banner">
    <div class="container">
      <div class="grid justify--between">
        <div class="grid__column six-twelfths mobile--one-whole">
          <div class="blogs-banner__image">
            <div class="swiper-container gallery-top">
              <div class="swiper-wrapper">
                <?php
                $args=array(
                  'post_type' => 'post',
                  'post__in' => $posts_list
                );

                $my_query = new wp_query($args);
                if( $my_query->have_posts() ) : ?>
                  <?php
                  while ($my_query->have_posts()) :
                    $my_query->the_post();
                    ?>
                    <div class="swiper-slide">
                      <div class="image">
                        <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php echo get_the_title(); ?>">
                      </div>
                    </div>
                  <?php endwhile; ?>
                <?php
                endif;
                wp_reset_query();
                ?>
              </div>
            </div>
          </div>
        </div>
        <div class="grid__column one-twelfth mobile--hidden"></div>
        <div class="grid__column five-twelfths mobile--one-whole">
          <div class="blogs-banner__content">
            <div class="swiper-container gallery-thumbs">
              <div class="swiper-wrapper">
                <?php
                $args=array(
                  'post_type' => 'post',
                  'post__in' => $posts_list
                );

                $my_query = new wp_query($args);
                if( $my_query->have_posts() ) : ?>
                  <?php
                  while ($my_query->have_posts()) :
                    $my_query->the_post();
                    ?>
                    <div class="swiper-slide">
                      <span class="blogs-banner__date"><?= get_the_date(); ?></span>
                      <div class="heading">
                        <h2 class="heading__title">
                          <a href="<?= get_the_permalink(); ?>"><?= get_the_title(); ?></a>
                        </h2>
                      </div>
                      <span class="blogs-banner__description">
                        <p><?= wp_trim_words( get_the_content(), 20, '...' ); ?></p>
                      </span>
                      <a href="<?= get_the_permalink(); ?>" class="readmore">
                        <?php esc_html_e('Read More', 'sport-floor'); ?>
                        <span><svg width="14" height="14" viewBox="0 0 14 14" fill="none"><path d="M11.1362 3.0488v5.0154m0-5.0154H6.1209m5.0153 0L2.6441 11.541" stroke="currentColor" stroke-width="1.5"/></svg></span>
                      </a>
                    </div>
                  <?php endwhile; ?>
                <?php
                endif;
                wp_reset_query();
                ?>
              </div>
            </div>
            <div class="swiper-pagination"></div>
            <div class="slide-arrow">
              <button class="slide-prev">
                <svg width="19" height="19" viewBox="0 0 19 19" fill="none"><path d="M1.9206 9.879l4.5597 4.5595M1.9207 9.8789l4.5596-4.5596M1.9207 9.8789h15.441" stroke="currentColor" stroke-width="1.5"/></svg>
              </button>
              <button class="slide-next">
                <svg width="19" height="19" viewBox="0 0 19 19" fill="none"><path d="M17.0792 9.8794l-4.5596 4.5596m4.5596-4.5596l-4.5596-4.5597m4.5596 4.5597H1.6382" stroke="currentColor" stroke-width="1.5"/></svg>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>
  <div class="grid justify--between">

  </div>
    <section class="section blogs-post">
        <div class="container">
            <div class="grid grid--three-columns grid--doubling">
                <?php
                $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                $args=array(
                    'posts_per_page' => 6,
                    'paged' => $paged
                );

                $my_query = new wp_query($args);

                $last_page = $my_query->max_num_pages;
                if( $my_query->have_posts() ) : ?>
                    <?php
                    while ($my_query->have_posts()) :
                        $my_query->the_post();
                        ?>
                        <div class="swiper-slide grid__column">
                            <?php echo get_template_part('template-parts/blog-grid'); ?>
                        </div>
                    <?php endwhile; ?>
                <?php
                endif;
                ?>
            </div>
            <?php
                echo sport_floor_posts_navigation($paged,$last_page);
                wp_reset_query();
            ?>
        </div>
    </section>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <!-- Initialize Swiper -->
    <script>
        let galleryThumbs = new Swiper('.gallery-thumbs', {
            spaceBetween: 10,
            slidesPerView: 1,
            freeMode: true,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
        });
        let galleryTop = new Swiper('.gallery-top', {
            spaceBetween: 10,
            navigation: {
                nextEl: '.slide-next',
                prevEl: '.slide-prev',
            },
            thumbs: {
                swiper: galleryThumbs
            },
            pagination: {
                el: '.swiper-pagination',
            },
        });
    </script>
<?php
get_footer();
