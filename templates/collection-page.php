<?php
/**
 * Template Name: Collections
 *
 * @package Sport floor
 */

get_header();

// Collection Setting

$collection_title = rwmb_meta( 'collection_title' );
$collection_description = rwmb_meta( 'collection_description' );
$collection_image = rwmb_meta( 'collection_image' );
$collection_intro = rwmb_meta( 'collection_intro' );
$collection_info_heading = rwmb_meta( 'collection_info_heading' );
$collection_info = rwmb_meta( 'collection_info' );
$collection_info_caro = rwmb_meta( 'collection_info_caro' );
$collection_list = rwmb_meta( 'collection_list' );
$collection_list_heading = rwmb_meta( 'collection_list_heading' );
?>
<section class="hero-banner page-banner">
  <?php if ( !empty($collection_image) ) : ?>
    <div class="hero-banner__image">
      <img src="<?php echo $collection_image['full_url']; ?>" alt="Collection banner">
    </div>
  <?php endif; ?>
  <?php if ( !empty($collection_title) or !empty($collection_description) ) : ?>
    <div class="hero-banner__content pdt--40">
      <div class="container">
        <?php if ( !empty($collection_title) ) : ?>
          <h1 class="text--center"><?= $collection_title; ?></h1>
        <?php endif; ?>
        <?php if ( !empty($collection_description) ) : ?>
          <p class="text--center text--white"><?= $collection_description; ?></p>
        <?php endif; ?>
      </div>
    </div>
  <?php endif; ?>
</section>
<?php if ( !empty($collection_intro) ) : ?>
  <section class="section section--orange collection-about">
    <div class="container">
      <div class="collection-about__container nine-twelfths mobile--one-whole">
        <h3><?= $collection_intro; ?></h3>
      </div>
    </div>
  </section>
<?php endif; ?>
<?php if ( !empty($collection_info) ) : ?>
  <section class="section collection-info <?php if ( !empty($collection_info_caro) ) : echo 'collection-info--caro'; endif; ?>">
    <div class="container">
      <?php if ( !empty($collection_info_heading) ) : ?>
        <div class="heading">
          <h2 class="heading__title text--center"><?= $collection_info_heading; ?></h2>
        </div>
      <?php endif; ?>
      <div class="collection-info__list">
        <?php foreach ($collection_info as $item) : ?>
          <?php $imgs = $item['image']; ?>
          <div class="grid grid--aligned-center mgb--50">
            <div class="grid__column six-twelfths mobile--one-whole">
              <?php
              foreach ( $imgs as $img ) {
                echo '<img src="' . wp_get_attachment_image_url( $img, 'full_url' ) . '" alt="What Make Us Different?">';
              }
              ?>
            </div>
            <div class="grid__column one-twelfth mobile--hidden"></div>
            <div class="grid__column four-twelfths mobile--one-whole">
              <?php if ( !empty($item['title']) ) : ?>
                <h2 class="heading__title"><?= $item['title']; ?></h2>
              <?php endif; ?>
              <?php if ( !empty($item['description']) ) : ?>
                <p><?= $item['description']; ?></p>
              <?php endif; ?>
            </div>
            <div class="grid__column one-twelfth"></div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </section>
<?php endif; ?>
<section class="section section--gray collection-fool">
    <div class="container">
      <?php if ( !empty($collection_list_heading) ) : ?>
        <div class="heading">
          <h2 class="heading__title text--center"><?= $collection_list_heading; ?></h2>
        </div>
      <?php endif; ?>
      <?php if (!empty($collection_list) && count($collection_list) > 0) : ?>
        <div class="grid grid--four-columns collection-products__grid">
          <?php
          $args=array(
            'post_type' => 'page',
            'post__in' => $collection_list
          );

          $my_query = new wp_query($args);
          if( $my_query->have_posts() ) : ?>
            <?php
            while ($my_query->have_posts()) :
              $my_query->the_post();
              ?>
              <div class="grid__column">
                <div class="collection-grid">
                  <div class="collection-grid__thumbnail">
                    <a href="<?php echo get_the_permalink(); ?>">
                      <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php echo get_the_title(); ?>">
                    </a>
                  </div>
                  <h4 class="collection-grid__title text--center">
                    <a href="<?php echo get_the_permalink(); ?>">
                      <?php echo get_the_title(); ?>
                    </a>
                  </h4>
                </div>
              </div>
            <?php endwhile; ?>
          <?php
          endif;
          wp_reset_query();
          ?>
        </div>
      <?php endif; ?>
    </div>
</section>
<?php
get_footer();
