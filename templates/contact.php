<?php
/**
 * Template Name: Contact
 *
 * @package Sport floor
 */

// Contact Page Setting
$contact_hero_image = rwmb_meta( 'contact_hero_image' );
$contact_form_title = rwmb_meta( 'contact_form_title' );
$contact_info_title = rwmb_meta( 'contact_info_title' );
$contact_address = rwmb_meta( 'contact_address' );
$contact_phone = rwmb_meta( 'contact_phone' );
$contact_fax = rwmb_meta( 'contact_fax' );
$contact_email = rwmb_meta( 'contact_email' );

echo get_template_part('header-2');
?>
<div class="contact-page">
  <?php if ( !empty($contact_hero_image) ) : ?>
    <div class="contact-page__header">
        <img src="<?php echo $contact_hero_image['full_url']; ?>" alt="<?= get_the_title(); ?>">
    </div>
  <?php endif; ?>
    <div class="contact-form">
        <div class="container">
            <div class="contact-form__wrapper">
                <div class="contact-form__info">
                  <?php if ( !empty($contact_info_title) ) : ?>
                    <div class="heading">
                        <h2 class="heading__title"><?= $contact_info_title; ?></h2>
                    </div>
                  <?php endif; ?>
                  <?php if ( !empty($contact_address) ) : ?>
                    <div class="contact-form__item">
                        <span class="contact-form__title"><?php esc_html_e('Address', 'sport-floor'); ?></span>
                        <div class="contact-form__description">
                            <p><?= $contact_address; ?></p>
                        </div>
                    </div>
                  <?php endif; ?>
                  <?php if ( !empty($contact_phone) || !empty($contact_fax) ) : ?>
                    <div class="contact-form__item">
                        <span class="contact-form__title"><?php esc_html_e('Phone', 'sport-floor'); ?></span>
                        <div class="contact-form__description">
                          <?php if ( !empty($contact_phone) ) : ?>
                            <a href="tel:<?= $contact_phone; ?>">Tel: <?= $contact_phone; ?></a>
                          <?php endif; ?>
                          <?php if ( !empty($contact_fax) ) : ?>
                            <a href="tel:<?= $contact_fax; ?>">Tel: <?= $contact_fax; ?></a>
                          <?php endif; ?>
                        </div>
                    </div>
                  <?php endif; ?>
                  <?php if ( !empty($contact_email) ) : ?>
                    <div class="contact-form__item">
                        <span class="contact-form__title"><?php esc_html_e('Email', 'sport-floor');?></span>
                        <div class="contact-form__description">
                            <a href="mailto:<?= $contact_email; ?>" class="text--primary"><?= $contact_email; ?></a>
                        </div>
                    </div>
                  <?php endif; ?>
                </div>
                <div class="contact-form__content">
                  <?php if ( !empty($contact_form_title) ) : ?>
                    <h3 class="contact-form__heading"><?= $contact_form_title; ?></h3>
                  <?php endif; ?>
                    <?php
                    while ( have_posts() ) :
                        the_post();

                        echo the_content();

                    endwhile;
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>
<?php
get_footer();
