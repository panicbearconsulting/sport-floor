<?php
/**
 * Template Name: About
 *
 * @package Sport floor
 */

// About Page Setting
$about_header_content = rwmb_meta( 'about_header_content' );
$about_hero_image = rwmb_meta( 'about_hero_image' );
$why_us_list = rwmb_meta( 'why_us_list' );
$why_us_sub_title = rwmb_meta( 'why_us_subtitle' );
$why_us_title = rwmb_meta( 'why_us_title' );
$why_us_image = rwmb_meta( 'why_us_image' );
$meet_our_team_heading = rwmb_meta( 'meet_our_team_heading' );
$meet_our_team_list = rwmb_meta( 'meet_our_team_list' );
$about_banner_text = rwmb_meta( 'about_banner_text' );

echo get_template_part('header-2');
?>
<div class="about-page">
    <section class="about-header">
        <div class="container">
            <div class="grid">
                <div class="grid__column six-twelfths mobile--one-whole">
                    <div class="heading">
                        <h1 class="h2 heading__title"><?= get_the_title(); ?></h1>
                    </div>
                </div>
                <div class="grid__column six-twelfths mobile--one-whole">
                  <?php if ( !empty($about_header_content) ) : ?>
                    <div class="about-header__content">
                        <p><?= $about_header_content; ?></p>
                    </div>
                  <?php endif; ?>
                </div>
            </div>
        </div>
      <?php if ( !empty($about_hero_image) ) : ?>
        <div class="about-banner">
          <img src="<?php echo $about_hero_image['full_url']; ?>" alt="<?= get_the_title(); ?>">
        </div>
      <?php endif; ?>
    </section>
  <?php if ( !empty($why_us_list) || !empty($why_us_sub_title) || !empty($why_us_title) || !empty($why_us_image) ) : ?>
    <section class="section about-why-us">
        <div class="container">
          <?php if ( !empty($why_us_sub_title) || !empty($why_us_title) ) : ?>
            <div class="heading">
              <?php if ( !empty($why_us_sub_title) ) : ?>
                <span class="heading__tag"><?= $why_us_sub_title; ?></span>
              <?php endif; ?>
              <?php if ( !empty($why_us_title) ) : ?>
                <h2 class="heading__title"><?= $why_us_title; ?></h2>
              <?php endif; ?>
            </div>
          <?php endif; ?>
          <?php if ( !empty($why_us_list) || !empty($why_us_image) ) : ?>
            <div class="grid justify--between">
                <div class="grid__column six-twelfths mobile--one-whole">
                  <?php if ( !empty($why_us_image) ) : ?>
                    <div class="why-us__thumbnail">
                        <img src="<?php echo $why_us_image['full_url']; ?>" alt="">
                    </div>
                  <?php endif; ?>
                </div>
                <div class="grid__column one-twelfth mobile--hidden"></div>
              <?php if ( !empty($why_us_list) ) : ?>
                <div class="grid__column five-twelfths mobile--one-whole">
                  <?php foreach ( $why_us_list as $item ) : ?>
                    <div class="why-us__item why-us__item--dark">
                      <?php if ( !empty($item['image']) ) : ?>
                        <div class="why-us__icon">
                          <?php echo '<img src="' . wp_get_attachment_image_url( $item['image'], 'full_url' ) . '" alt="">'; ?>
                        </div>
                      <?php endif; ?>
                      <?php if ( !empty($item['title']) || !empty($item['description']) ) : ?>
                        <div class="why-us__content">
                          <?php if ( !empty($item['title']) ) : ?>
                            <h4><?= $item['title']; ?></h4>
                          <?php endif; ?>
                          <?php if ( !empty($item['description']) ) : ?>
                            <p class="text--white"><?= $item['description']; ?></p>
                          <?php endif; ?>
                        </div>
                      <?php endif; ?>
                    </div>
                  <?php endforeach; ?>
                </div>
              <?php endif; ?>
            </div>
          <?php endif; ?>
        </div>
    </section>
  <?php endif; ?>
  <?php if ( !empty($meet_our_team_heading) || !empty($meet_our_team_list) ) : ?>
    <section class="section section--dark">
        <div class="container">
          <?php if ( !empty($meet_our_team_heading) ) : ?>
            <div class="heading heading--white">
                <h2 class="heading__title"><?= $meet_our_team_heading; ?></h2>
            </div>
          <?php endif; ?>
          <?php if ( !empty($meet_our_team_list) ) : ?>
            <div class="grid grid--three-columns grid--doubling grid--box">
              <?php foreach ( $meet_our_team_list as $item ) : ?>
                <div class="grid__column">
                  <div class="team-grid">
                    <?php if ( !empty($item['image']) ) : ?>
                      <div class="team-grid__thumbnail">
                        <?php echo '<img src="' . wp_get_attachment_image_url( $item['image'], 'full_url' ) . '" alt="">'; ?>
                      </div>
                    <?php endif; ?>
                    <?php if ( !empty($item['title']) ) : ?>
                      <h4 class="team-grid__title"><?= $item['title']; ?></h4>
                    <?php endif; ?>
                    <?php if ( !empty($item['job']) ) : ?>
                      <span class="team-grid__position"><?= $item['job']; ?></span>
                    <?php endif; ?>
                    <?php if ( !empty($item['description']) ) : ?>
                      <div class="team-grid__description"><?= $item['description']; ?></div>
                    <?php endif; ?>
                  </div>
                </div>
              <?php endforeach; ?>
            </div>
          <?php endif; ?>
        </div>
    </section>
  <?php endif; ?>
  <?php if ( !empty($about_banner_text) ) : ?>
    <section class="section section--primary">
        <div class="container">
            <div class="heading heading--white">
                <h2 class="heading__title text--center"><?= $about_banner_text; ?></h2>
            </div>
        </div>
    </section>
  <?php endif; ?>
</div>
<?php
get_footer();
