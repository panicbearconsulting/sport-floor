<?php
/**
 * Template Name: Homepage
 *
 * @package Sport floor
 */

get_header();


// Home setting

$prefix = 'sport_floor';

// Banner
$home_title = rwmb_meta( $prefix . 'home_title' );
$home_description = rwmb_meta( $prefix . 'home_description' );
$home_link = rwmb_meta( $prefix . 'home_link' );
$home_banner = rwmb_meta( $prefix . 'home_banner' );
$home_banner = !empty($home_banner['full_url']) ? $home_banner['full_url'] : '';

//// Product category
$product_category_heading = rwmb_meta( $prefix . 'product_category_heading' );
$product_category_page = rwmb_meta( $prefix . 'product_category_page' );
//
//// MAINTENANCE AND CARE
$mac_title = rwmb_meta( $prefix . 'mac_title' );
$mac_heading = rwmb_meta( $prefix . 'mac_heading' );
$mac_list = rwmb_meta( $prefix . 'mac_list' );
$mac_banner = rwmb_meta( $prefix . 'mac_banner' );
$mac_banner = !empty($mac_banner['full_url']) ? $mac_banner['full_url'] : '';

//// WHY US
$why_title = rwmb_meta( $prefix . 'why_title' );
$why_heading = rwmb_meta( $prefix . 'why_heading' );
$why_list = rwmb_meta( $prefix . 'why_list' );
$why_banner = rwmb_meta( $prefix . 'why_banner' );
$why_banner = !empty($why_banner['full_url']) ? $why_banner['full_url'] : '';

//// BLOG
$blog_title = rwmb_meta( $prefix . 'blog_title' );
$blog_heading = rwmb_meta( $prefix . 'blog_heading' );

//// Description
$description_heading = rwmb_meta( $prefix . 'description_heading' );
?>
<section class="hero-banner">
    <div class="hero-banner__image">
        <?php if (!empty($home_banner) ) : ?>
            <img src="<?php echo $home_banner; ?>" alt="Home banner">
        <?php else : ?>
            <img src="<?php echo get_template_directory_uri() . '/images/home-banner.jpg'; ?>" alt="Home banner">
        <?php endif; ?>
    </div>
    <div class="hero-banner__content pdt--40">
        <div class="container">
            <?php print !empty($home_title) ? '<h1>'. $home_title .'</h1>' : ''; ?>
            <?php print !empty($home_description) ? '<p class="text--white">'. $home_description .'</p>' : ''; ?>
          <a href="<?php echo sport_floor_get_page_id('templates/contact'); ?>" class="btn btn--primary"><?php esc_html_e('Get Free Consultation', 'sport-floor');?></a>
        </div>
    </div>
</section>
<section class="section section--collection">
    <div class="container">
        <?php if(!empty($product_category_heading)) : ?>
            <div class="heading">
                <h2 class="heading__title text--center">
                    <?php echo $product_category_heading; ?>
                </h2>
            </div>
        <?php endif; ?>
        <?php if (!empty($product_category_page) && count($product_category_page) > 0) : ?>
        <div class="grid grid--three-columns grid--doubling">

            <?php
            $args=array(
                'post_type' => 'page',
                'post__in' => $product_category_page
            );

            $my_query = new wp_query($args);
            if( $my_query->have_posts() ) : ?>
                <?php
                while ($my_query->have_posts()) :
                    $my_query->the_post();
                    ?>
                    <div class="grid__column">
                        <div class="collection-grid">
                            <div class="collection-grid__thumbnail">
                                <a href="<?php echo get_the_permalink(); ?>">
                                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php echo get_the_title(); ?>">
                                </a>
                            </div>
                            <h4 class="collection-grid__title">
                                <a href="<?php echo get_the_permalink(); ?>">
                                    <?php echo get_the_title(); ?>
                                </a>
                            </h4>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php
            endif;
            wp_reset_query();
            ?>
        </div>
        <?php endif; ?>
    </div>
</section>
<section class="section section--gray home-services">
    <div class="container">
        <div class="eight-twelfths mobile--one-whole">
            <div class="heading">
                <?php print !empty($mac_title) ? '<span class="heading__tag">' . $mac_title . '</span>' : ''; ?>
                <?php print !empty($mac_heading) ? '<h2 class="heading__title">' . $mac_heading . '</h2>' : ''; ?>
            </div>
            <?php if (!empty($mac_list)) : ?>
            <div class="grid grid--two-columns grid--doubling">
                <?php foreach ($mac_list as $mac_item) : ?>
                    <div class="grid__column">
                        <div class="home-services__item">
                            <?php echo !empty($mac_item[ $prefix . 'title']) ? '<h3>'. $mac_item[ $prefix . 'title'] .'</h3>' : ''; ?>
                            <?php echo !empty($mac_item[ $prefix . 'content']) ? '<p>'. $mac_item[ $prefix . 'content'] .'</p>' : ''; ?>
                            <?php if (!empty($mac_item[ $prefix . 'link'])) : ?>
                                <a href="<?php echo get_permalink($mac_item[ $prefix . 'link']); ?>" class="readmore">
                                    Learn more
                                    <span>
                                        <svg width="14" height="14" viewBox="0 0 14 14" fill="none"><path d="M11.1362 3.0488v5.0154m0-5.0154H6.1209m5.0153 0L2.6441 11.541" stroke="currentColor" stroke-width="1.5"/></svg>
                                    </span>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <?php if (!empty($mac_banner)) : ?>
    <div class="home-services__image">
        <img src="<?php echo $mac_banner; ?>" alt="<?php echo !empty($mac_title) ?  $mac_title : 'MAC' ?>">
    </div>
    <?php endif; ?>
</section>
<section class="section section--dark home-why-us">
    <div class="container">
        <div class="heading heading--white nine-twelfths mobile--one-whole">
            <?php print !empty($why_title) ? '<span class="heading__tag">' . $why_title . '</span>' : ''; ?>
            <?php print !empty($why_heading) ? '<h2 class="heading__title">' . $why_heading . '</h2>' : ''; ?>
        </div>
        <div class="grid justify--between">
            <div class="grid__column six-twelfths mobile--one-whole">
                <?php if (!empty($home_link)) : ?>
                <div class="tablet--hidden large--hidden desk--hidden">
                    <a href="<?php echo sport_floor_get_page_id('templates/contact'); ?>" class="btn one-whole mgb--50"><?php esc_html_e('Get Free Consultation', 'sport-floor');?></a>
                </div>
                <?php endif; ?>
                <?php if (!empty($why_banner)) : ?>
                    <div class="home-why-us__image">
                        <img src="<?php echo $why_banner; ?>" alt="<?php echo !empty($why_title) ? $why_list : 'WHY US?'; ?>">
                    </div>
                <?php endif; ?>
            </div>
            <div class="grid__column one-twelfth mobile--hidden"></div>
            <?php if (!empty($why_list)) : ?>
                <div class="grid__column five-twelfths mobile--one-whole">
                    <?php foreach ($why_list as $why_item) : ?>
                    <div class="why-us__item">
                        <?php if (!empty($why_item[ $prefix . 'icon' ])) : ?>
                            <div class="why-us__icon">
                                <?php print  $why_item[ $prefix . 'icon' ]; ?>
                            </div>
                        <?php endif; ?>
                        <div class="why-us__content">
                            <?php echo !empty($why_item[ $prefix . 'title']) ? '<h4>'. $why_item[ $prefix . 'title'] .'</h4>' : ''; ?>
                            <?php echo !empty($why_item[ $prefix . 'description']) ? '<p class="text--white">'. $why_item[ $prefix . 'description'] .'</p>' : ''; ?>
                        </div>
                    </div>
                    <?php endforeach; ?>
                  <a href="<?php echo sport_floor_get_page_id('templates/contact'); ?>" class="btn mobile--hidden why-us__btn"><?php esc_html_e('Get Free Consultation', 'sport-floor');?></a>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<section class="section home-blog">
    <div class="container">
        <div class="heading nine-twelfths mobile--one-whole tablet--one-whole">
            <?php print !empty($blog_title) ? '<span class="heading__tag">' . $blog_title . '</span>' : ''; ?>
            <?php print !empty($blog_heading) ? '<h2 class="heading__title">' . $blog_heading . '</h2>' : ''; ?>
        </div>
        <div class="home-blog__slider swiper-container" style="margin-left: -15px;margin-right: -15px;">
            <div class="swiper-wrapper">
                <?php
                $args=array(
                    'showposts'=>9,
                    'caller_get_posts'=>1
                );

                $my_query = new wp_query($args);
                if( $my_query->have_posts() ) : ?>
                <?php
                    while ($my_query->have_posts()) :
                    $my_query->the_post();
                    ?>
                        <div class="swiper-slide grid__column">
                            <?php echo get_template_part('template-parts/blog-grid'); ?>
                        </div>
                    <?php endwhile; ?>
                <?php
                endif;
                wp_reset_query();
                ?>
            </div>
        </div>
        <div class="home-blog__action">
            <div class="slide-arrow">
                <button class="slide-prev">
                    <svg width="19" height="19" viewBox="0 0 19 19" fill="none"><path d="M1.9206 9.879l4.5597 4.5595M1.9207 9.8789l4.5596-4.5596M1.9207 9.8789h15.441" stroke="currentColor" stroke-width="1.5"/></svg>
                </button>
                <button class="slide-next">
                    <svg width="19" height="19" viewBox="0 0 19 19" fill="none"><path d="M17.0792 9.8794l-4.5596 4.5596m4.5596-4.5596l-4.5596-4.5597m4.5596 4.5597H1.6382" stroke="currentColor" stroke-width="1.5"/></svg>
                </button>
            </div>
            <a href="./blogs" class="btn btn--outline mobile--one-whole">View All</a>
        </div>
    </div>
</section>
<?php if (!empty($description_heading)) : ?>
<section class="section section--primary">
    <div class="container">
        <div class="heading heading--white text--center">
            <h2 class="heading__title">
                <?php echo $description_heading; ?>
            </h2>
        </div>
    </div>
</section>
<?php endif; ?>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<?php
get_footer();
