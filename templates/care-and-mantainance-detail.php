<?php
/**
 * Template Name: Care and Mantainance Detail
 *
 * @package Sport floor
 */

get_header();

// Care & Maintenance
$care_sub_heading = rwmb_meta( 'care_sub_heading' );
$care_heading = rwmb_meta( 'care_heading' );
$care_description = rwmb_meta( 'care_description' );
$care_image = rwmb_meta( 'care_image' );

$care_menu_title = rwmb_meta('care_menu_title');
$care_menu_cta_text = rwmb_meta('care_menu_cta_text');
$care_menu_cta_url = rwmb_meta('care_menu_cta_url');

$care_about = rwmb_meta('care_about');

$care_features_image = rwmb_meta('care_features_image');
$care_features_list = rwmb_meta('care_features_list');

$care_featured_product_heading = rwmb_meta('care_featured_product_heading');
$care_featured_products = rwmb_meta('care_featured_products');

$care_contact_heading = rwmb_meta('care_contact_heading');
$care_contact_cta_text = rwmb_meta('care_contact_cta_text');
$care_contact_cta_url = rwmb_meta('care_contact_cta_url');
$care_contact_image = rwmb_meta('care_contact_image');
?>
<?php if ( !empty($care_sub_heading) or !empty($care_heading) or !empty($care_description) ) : ?>
  <section class="hero-banner hero-banner--center page-banner">
    <?php if ( !empty($care_image) ) : ?>
      <div class="hero-banner__image">
        <img src="<?php echo $care_image['full_url']; ?>" alt="CARE & MAINTENANCE">
      </div>
    <?php endif; ?>
    <div class="hero-banner__content pdt--40">
      <div class="container">
        <?php if ( !empty($care_sub_heading) ) : ?>
          <span class="sub-heading"><?= $care_sub_heading; ?></span>
        <?php endif; ?>
        <?php if ( !empty($care_heading) ) : ?>
          <h1><?= $care_heading; ?></h1>
        <?php endif; ?>
        <?php if ( !empty($care_description) ) : ?>
        <p class="text--white"><?= $care_description; ?></p>
        <?php endif; ?>
      </div>
    </div>
  </section>
<?php endif; ?>
<?php if ( !empty($care_menu_title) or (!empty($care_menu_cta_text) and !empty($care_menu_cta_url)) ) : ?>
  <div class="detail-menu">
    <div class="detail-menu__wrapper">
      <div class="container">
        <div class="flex justify--between align--center">
          <?php if ( !empty($care_menu_title) ) : ?>
            <h5 class="text--white text--bold no-style"><?= $care_menu_title; ?></h5>
          <?php endif; ?>
          <?php if ( !empty($care_menu_cta_text) and !empty($care_menu_cta_url) ) : ?>
            <a href="<?= get_permalink($care_menu_cta_url); ?>" class="btn"><?= $care_menu_cta_text; ?></a>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php if ( !empty($care_about) ) : ?>
  <section class="section section--orange about-care">
    <div class="container">
      <p class="h4 text--center"><?= $care_about; ?></p>
    </div>
  </section>
<?php endif; ?>
<?php if ( !empty($care_features_image) or !empty($care_features_list) ) : ?>
  <section class="section tabs-section">
    <div class="container">
      <div class="grid justify--between">
        <?php if ( !empty($care_features_image) ) : ?>
          <div class="grid__column six-twelfths mobile--hidden">
            <img src="<?php echo $care_features_image['full_url'] ?> " alt="">
          </div>
        <?php endif; ?>
        <?php if ( !empty($care_features_list) ) : ?>
          <div class="grid__column five-twelfths mobile--one-whole">
            <div class="tabs">
              <?php foreach( $care_features_list as $item ) : ?>
                <div class="tabs__item">
                  <a href="#" class="tabs__header" data-tab-active><?= $item['title']; ?></a>
                  <div class="tabs__content" data-tab-content>
                    <p><?= $item['description']; ?></p>
                  </div>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </section>
<?php endif; ?>
<?php if ( !empty($care_featured_product_heading) or !empty($care_featured_products) ) : ?>
  <section class="section section--gray featureds">
    <div class="container">
      <?php if ( !empty($care_featured_product_heading) ) : ?>
        <div class="heading">
          <h2 class="heading__title text--center"><?= $care_featured_product_heading; ?></h2>
        </div>
      <?php endif; ?>
      <?php if ( !empty($care_featured_products) ) : ?>
        <div class="grid grid--four-columns justify--center grid--doubling">
          <?php
          $args=array(
            'post_type' => 'page',
            'post__in' => $care_featured_products
          );

          $my_query = new wp_query($args);
          if( $my_query->have_posts() ) : ?>
            <?php
            while ($my_query->have_posts()) :
              $my_query->the_post();
              ?>
              <div class="grid__column">
                <div class="product-card">
                  <div class="product-card__thumbnail">
                    <a href="<?php echo get_the_permalink(); ?>">
                      <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php echo get_the_title(); ?>">
                    </a>
                  </div>
                  <h4 class="product-card__title">
                    <a href="<?php echo get_the_permalink(); ?>">
                      <?php echo get_the_title(); ?>
                    </a>
                  </h4>
                </div>
              </div>
            <?php endwhile; ?>
          <?php
          endif;
          wp_reset_query();
          ?>
        </div>
      <?php endif; ?>
    </div>
  </section>
<?php endif; ?>
<?php if ( !empty($care_contact_heading) or (!empty($care_contact_cta_text) and !empty($care_contact_cta_url)) or !empty($care_contact_image) ) : ?>
  <section class="section detail-contact-us mgt--100 mobile--hidden">
    <div class="container">
      <div class="grid grid--aligned-center">
        <?php if ( !empty($care_contact_heading) or (!empty($care_contact_cta_text) and !empty($care_contact_cta_url)) ) : ?>
          <div class="grid__column five-twelfths">
            <?php if ( !empty($care_contact_heading) ) : ?>
              <div class="heading">
                <h2 class="heading__title"><?= $care_contact_heading; ?></h2>
              </div>
            <?php endif; ?>
            <?php if ( !empty($care_contact_cta_text) and !empty($care_contact_cta_url) ) : ?>
              <a href="<?= get_permalink($care_contact_cta_url); ?>" class="btn"><?= $care_contact_cta_text; ?></a>
            <?php endif; ?>
          </div>
        <?php endif; ?>
        <?php if ( !empty($care_contact_image) ) : ?>
          <div class="grid__column seven-twelfths">
            <img src="<?php echo $care_contact_image['full_url']; ?>" alt="How to clean hardwood floors the right way">
          </div>
        <?php endif; ?>
      </div>
    </div>
  </section>
<?php endif; ?>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<?php
get_footer();
