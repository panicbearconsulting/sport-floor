<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Sport_floor
 */

echo get_template_part('/header-2');

$post_id = get_the_ID();
$product = wc_get_product( $post_id );
$product_gallery = $product->get_gallery_image_ids();

?>
    <div class="product-detail">
        <div class="container">
            <div class="product-detail__summary">
                <div class="grid">
                    <div class="grid__column six-twelfths mobile--one-whole">
                        <div class="swiper-container product-detail__slider">
                            <div class="swiper-wrapper">
                              <?php foreach ( $product_gallery as $gallery_id ) : ?>
                                <div class="swiper-slide">
                                  <div class="product-detail__thumbnails">
                                    <img src="<?php echo wp_get_attachment_url( $gallery_id ); ?>" alt="Product gallery">
                                  </div>
                                </div>
                              <?php endforeach; ?>
                            </div>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                    </div>
                    <div class="grid__column six-twelfths mobile--one-whole">
                        <div class="product-detail__info">
                            <div class="breadcrumb">
                              <?php echo woocommerce_breadcrumb(); ?>
                            </div>
                            <h1 class="h3 product-detail__title">
                                <?php echo get_the_title(); ?>
                            </h1>
                            <?php $regular_price = $product->get_regular_price();?>
                            <span class="product-detail__price">
                                <?php echo $product->get_price(); ?>
                                <?php echo !empty($regular_price) ? '<del>' . $regular_price . '$</del>' : '' ; ?>
                            </span>
                            <div class="product-detail__description">
                                <?php echo $product->get_description(); ?>
                            </div>

                            <?php
                            if ( $product->has_dimensions() ) {
                              echo '<div class="product-detail__variant">
                                <span class="product-detail__variant-label">' . __( 'SIZE', 'sport-floor' ) . '</span>
                                <span class="product-detail__variant-value">' . esc_html( wc_format_dimensions( $product->get_dimensions( false ) ) ) . '</span>
                              </div>';
                            }
                            ?>

                          <form class="cart product-form__cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
                            <?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
                            <div class="product-form__quantity">
                              <?php
                              do_action( 'woocommerce_before_add_to_cart_quantity' );

                              woocommerce_quantity_input(
                                array(
                                  'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
                                  'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
                                  'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(),
                                )
                              );

                              do_action( 'woocommerce_after_add_to_cart_quantity' );
                              ?>
                            </div>
                            <button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button btn"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

                            <?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
                          </form>
                          <?php woocommerce_output_all_notices(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product-detail__related">
            <div class="container">
              <?php
              echo woocommerce_output_related_products();
              ?>
            </div>
        </div>
        <div class="product-detail__policy">
            <div class="container">
                <div class="grid grid--three-columns grid--doubling">
                    <div class="grid__column">
                        <div class="policy-item">
                            <div class="policy-icon">
                                <svg width="32" height="32" viewBox="0 0 32 32" fill="none"><path fill-rule="evenodd" clip-rule="evenodd" d="M27.84 11.8294L30.2907 13.8c.656.5334 1.04 1.3334 1.04 2.1787a2.791 2.791 0 01-1.0427 2.1747l-2.4506 1.9706c-.388.312-.5894.8-.5334 1.2947l.34 3.128a2.7879 2.7879 0 01-3.0773 3.076l-3.1267-.34a1.4591 1.4591 0 00-1.2946.5333l-1.9707 2.4507A2.79 2.79 0 0116 31.3085a2.7902 2.7902 0 01-2.1746-1.0418L11.852 27.812c-.312-.3866-.8-.588-1.2946-.5333l-3.128.34a2.7898 2.7898 0 01-2.935-1.8481 2.7893 2.7893 0 01-.141-1.2292l.34-3.1267A1.4509 1.4509 0 004.16 20.12l-2.4506-1.9706A2.7885 2.7885 0 01.666 15.9747 2.788 2.788 0 011.7094 13.8l2.4533-1.972c.388-.3106.5893-.8.5333-1.2946l-.34-3.1267a2.7867 2.7867 0 013.0774-3.076l3.1266.336c.4934.056.984-.144 1.2947-.5333l1.9707-2.4494A2.8735 2.8735 0 0116 .6894a2.8744 2.8744 0 012.1747.9946l1.9707 2.4494c.312.3866.8.588 1.2946.5333l3.1307-.336a2.7879 2.7879 0 013.076 3.0773l-.34 3.1267a1.4579 1.4579 0 00.5333 1.2947zm-13.0253 9.16a2.38 2.38 0 001.7187-.9254l5.4226-7.2293a1.3339 1.3339 0 00.2533-.9886 1.3337 1.3337 0 00-.989-1.103 1.332 1.332 0 00-1.0102.1443 1.3332 1.3332 0 00-.3874.3473l-5.212 6.9467-3.0013-3a1.3323 1.3323 0 00-1.464-.3078 1.3327 1.3327 0 00-.7291.7292 1.3329 1.3329 0 00.3077 1.4639l3.2587 3.2507a2.378 2.378 0 001.832.6706v.0014z" fill="#F16622"/></svg>
                            </div>
                            <span class="policy-title">Long Time Warranty</span>
                        </div>
                    </div>
                    <div class="grid__column">
                        <div class="policy-item">
                            <div class="policy-icon">
                                <svg width="32" height="32" viewBox="0 0 32 32" fill="none"><path d="M18 3.5H1.5a1 1 0 00-1 1V24a1 1 0 001 1h2a4.5 4.5 0 119 0h6a.5004.5004 0 00.3536-.1464A.5004.5004 0 0019 24.5v-20a1.0002 1.0002 0 00-1-1z" fill="#F16622"/><path d="M8 28.5a3.5 3.5 0 113.5-3.5A3.5048 3.5048 0 018 28.5zm0-5a1.5002 1.5002 0 00-1.0607 2.5607 1.4993 1.4993 0 001.6347.3251 1.5002 1.5002 0 00.4867-2.4465A1.5 1.5 0 008 23.5zM24 28.5a3.5 3.5 0 113.5-3.5 3.5048 3.5048 0 01-1.0264 2.4736A3.5048 3.5048 0 0124 28.5zm0-5a1.5003 1.5003 0 00-1.3858.926A1.5 1.5 0 1024 23.5z" fill="#F16622"/><path d="M31.45 15.7765l-3.5-7A.5.5 0 0027.5 8.5h-7a.5002.5002 0 00-.5.5v13.9445a4.4998 4.4998 0 017.5344-.7238A4.5 4.5 0 0128.5 25H31a.5004.5004 0 00.3536-.1464A.5004.5004 0 0031.5 24.5V16a.501.501 0 00-.05-.2235zM21.5 15.5V10h5.382l2.75 5.5H21.5z" fill="#F16622"/></svg>
                            </div>
                            <span class="policy-title">
                                Free delivery nationalwide for<br>
                                order above $1000
                            </span>
                        </div>
                    </div>
                    <div class="grid__column">
                        <div class="policy-item">
                            <div class="policy-icon">
                                <svg width="32" height="32" viewBox="0 0 32 32" fill="none"><path d="M16.0003.6667A15.2033 15.2033 0 007.113 3.5053L4.9337 1.6567a.6667.6667 0 00-1.0927.4253L2.9123 9.46a.6666.6666 0 00.8407.7253l7.2473-2.0186a.6664.6664 0 00.4726-.4993.667.667 0 00-.2199-.6513L9.2317 5.3032A12.6601 12.6601 0 113.3337 16 1.3334 1.3334 0 00.667 16a15.3336 15.3336 0 009.4655 14.1662A15.3332 15.3332 0 0031.039 18.9914 15.3332 15.3332 0 0016.0003.6667z" fill="#F16622"/><path d="M12.4373 20.0574l-.6473-.1614a.3336.3336 0 00-.404.2427l-.3234 1.2933a.3336.3336 0 00.2427.404l.6467.162a15.2217 15.2217 0 003.3813.4914v2.1773h1.3333v-2.2413a5.246 5.246 0 002.6527-1.01A3.467 3.467 0 0020.6666 18.6c-.032-2.2-2.3333-3.3126-4-3.7033v-3.884a9.7074 9.7074 0 012.1394.3093l.6406.1847a.3338.3338 0 00.353-.1124.3335.3335 0 00.0597-.1156l.37-1.2787a.3345.3345 0 00-.0282-.254.334.334 0 00-.1998-.1593l-.6407-.1847a11.7252 11.7252 0 00-2.694-.386V7.3334h-1.3333v1.7333a5.4987 5.4987 0 00-2.3447.7433 3.3125 3.3125 0 00-1.5333 2.3387c-.432 2.74 2.114 4.078 3.876 4.5087v3.826a13.439 13.439 0 01-2.894-.426zm6.2293-1.432a1.4662 1.4662 0 01-.5893 1.222 3.112 3.112 0 01-1.4107.5526v-3.4313c.92.298 1.9887.8593 2 1.6567zm-5.2346-6.1587a1.3547 1.3547 0 01.5606-.918 3.1667 3.1667 0 011.3407-.4667v3.4847c-.8667-.3-2.0847-.9273-1.9013-2.1z" fill="#F16622"/></svg>
                            </div>
                            <span class="policy-title">Easy return</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<?php
get_footer();
