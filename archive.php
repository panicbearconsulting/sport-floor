<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Sport_floor
 */

echo get_template_part('header-2');
?>
    <section class="blogs-banner">
        <div class="container">
            <div class="grid justify--between">
                <div class="grid__column six-twelfths mobile--one-whole">
                    <div class="blogs-banner__image">
                        <div class="swiper-container gallery-top">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="image">
                                        <img src="<?php echo get_template_directory_uri() . '/images/blogs-1.jpg'; ?>" alt="How to clean hardwood floors the right way">
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="image">
                                        <img src="<?php echo get_template_directory_uri() . '/images/blogs-1.jpg'; ?>" alt="How to clean hardwood floors the right way">
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="image">
                                        <img src="<?php echo get_template_directory_uri() . '/images/blogs-1.jpg'; ?>" alt="How to clean hardwood floors the right way">
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="image">
                                        <img src="<?php echo get_template_directory_uri() . '/images/blogs-1.jpg'; ?>" alt="How to clean hardwood floors the right way">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid__column one-twelfth mobile--hidden"></div>
                <div class="grid__column five-twelfths mobile--one-whole">
                    <div class="blogs-banner__content">
                        <div class="swiper-container gallery-thumbs">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <span class="blogs-banner__date">
                                        17 Jan 2020
                                    </span>
                                    <div class="heading">
                                        <h2 class="heading__title">
                                            <a href="#">How to clean hardwood floors the right way</a>
                                        </h2>
                                    </div>
                                    <span class="blogs-banner__description">
                                        <p>
                                            Hardwood floors add a beautiful touch to just about any room, but there's some debate about the best way to clean them.
                                        </p>
                                    </span>
                                    <a href="#" class="readmore">
                                        Read more
                                        <span>
                                            <svg width="14" height="14" viewBox="0 0 14 14" fill="none"><path d="M11.1362 3.0488v5.0154m0-5.0154H6.1209m5.0153 0L2.6441 11.541" stroke="currentColor" stroke-width="1.5"/></svg>
                                        </span>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <div class="heading">
                                    <span class="blogs-banner__date">
                                        17 Jan 2020
                                    </span>
                                        <h2 class="heading__title">
                                            <a href="#">How to clean hardwood floors the right way</a>
                                        </h2>
                                    </div>
                                    <span class="blogs-banner__description">
                                        <p>
                                            Hardwood floors add a beautiful touch to just about any room, but there's some debate about the best way to clean them.
                                        </p>
                                    </span>
                                    <a href="#" class="readmore">
                                        Read more
                                        <span>
                                            <svg width="14" height="14" viewBox="0 0 14 14" fill="none"><path d="M11.1362 3.0488v5.0154m0-5.0154H6.1209m5.0153 0L2.6441 11.541" stroke="currentColor" stroke-width="1.5"/></svg>
                                        </span>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <div class="heading">
                                    <span class="blogs-banner__date">
                                        17 Jan 2020
                                    </span>
                                        <h2 class="heading__title">
                                            <a href="#">How to clean hardwood floors the right way</a>
                                        </h2>
                                    </div>
                                    <span class="blogs-banner__description">
                                        <p>
                                            Hardwood floors add a beautiful touch to just about any room, but there's some debate about the best way to clean them.
                                        </p>
                                    </span>
                                    <a href="#" class="readmore">
                                        Read more
                                        <span>
                                            <svg width="14" height="14" viewBox="0 0 14 14" fill="none"><path d="M11.1362 3.0488v5.0154m0-5.0154H6.1209m5.0153 0L2.6441 11.541" stroke="currentColor" stroke-width="1.5"/></svg>
                                        </span>
                                    </a>
                                </div>
                                <div class="swiper-slide">
                                    <div class="heading">
                                    <span class="blogs-banner__date">
                                        17 Jan 2020
                                    </span>
                                        <h2 class="heading__title">
                                            <a href="#">How to clean hardwood floors the right way</a>
                                        </h2>
                                    </div>
                                    <span class="blogs-banner__description">
                                        <p>
                                            Hardwood floors add a beautiful touch to just about any room, but there's some debate about the best way to clean them.
                                        </p>
                                    </span>
                                    <a href="#" class="readmore">
                                        Read more
                                        <span>
                                            <svg width="14" height="14" viewBox="0 0 14 14" fill="none"><path d="M11.1362 3.0488v5.0154m0-5.0154H6.1209m5.0153 0L2.6441 11.541" stroke="currentColor" stroke-width="1.5"/></svg>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-pagination"></div>
                        <div class="slide-arrow">
                            <button class="slide-prev">
                                <svg width="19" height="19" viewBox="0 0 19 19" fill="none"><path d="M1.9206 9.879l4.5597 4.5595M1.9207 9.8789l4.5596-4.5596M1.9207 9.8789h15.441" stroke="currentColor" stroke-width="1.5"/></svg>
                            </button>
                            <button class="slide-next">
                                <svg width="19" height="19" viewBox="0 0 19 19" fill="none"><path d="M17.0792 9.8794l-4.5596 4.5596m4.5596-4.5596l-4.5596-4.5597m4.5596 4.5597H1.6382" stroke="currentColor" stroke-width="1.5"/></svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section blogs-post">
        <div class="container">
            <div class="grid grid--three-columns grid--doubling">

                <?php if ( have_posts() ) : ?>

                    <?php
                    /* Start the Loop */
                    while ( have_posts() ) :
                        the_post();?>
                        <div class="grid__column">
                            <?php echo get_template_part('template-parts/blog-grid'); ?>
                        </div>
                    <?php
                    endwhile;


                endif;
                ?>
            </div>
            <?php
                echo sport_floor_posts_navigation();
            ?>
        </div>
    </section>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <!-- Initialize Swiper -->
    <script>
        let galleryThumbs = new Swiper('.gallery-thumbs', {
            spaceBetween: 10,
            slidesPerView: 1,
            freeMode: true,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
        });
        let galleryTop = new Swiper('.gallery-top', {
            spaceBetween: 10,
            navigation: {
                nextEl: '.slide-next',
                prevEl: '.slide-prev',
            },
            thumbs: {
                swiper: galleryThumbs
            },
            pagination: {
                el: '.swiper-pagination',
            },
        });
    </script>
<?php
get_footer(); ?>
