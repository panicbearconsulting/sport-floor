<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Sport_floor
 */

get_template_part('header-2');

$title = get_the_title();
$url = get_the_permalink();
$summary_text_with_url = sprintf(esc_html__('Visit %s on %s', 'sport-floor'), urlencode(get_the_title()), $url);
$summary_text_with_no_url = sprintf(esc_html__('Visit %s', 'sport-floor'), get_the_title());
?>

<?php
    while ( have_posts() ) :
    the_post();
?>
    <div class="blog-detail">
        <div class="container">
            <div class="blog-detail__container eight-twelfths mobile--one-whole">
                <div class="heading">
                <span class="blog-detail__date">
                    <?php echo get_the_date(); ?>
                </span>
                    <h1 class="h2 heading__title text--center">
                        <?php echo get_the_title() ;?>
                    </h1>
                </div>
                <div class="blog-detail__thumbnail">
                    <?php sport_floor_post_thumbnail(); ?>
                </div>
                <div class="blog-detail__content">
                    <?php the_content(); ?>
                </div>
            </div>
            <div class="blog-social">
            <span class="blog-social__title">
                Share this post
            </span>
                <div class="blog-social__icons">
                  <!-- Facebook -->
                  <a href="http://m.facebook.com/sharer.php?s=100&amp;p=<?php echo $title; ?>&amp;p[summary]=<?php echo $summary_text_with_no_url; ?>&amp;u=<?php echo $url; ?>" onclick="window.open(this.href, 'facebookwindow','left=20,top=20,width=600,height=700,toolbar=0,resizable=1'); return false;" title="<?php esc_html_e('Share on Facebook', 'sport-floor');?>">
                    <svg width="10" height="16" viewBox="0 0 10 16" fill="none"><path d="M3.023 16L3 9H0V6h3V4c0-2.6992 1.6715-4 4.0794-4C8.2328 0 9.224.0859 9.5129.1242v2.8209l-1.67.0007c-1.3095 0-1.563.6223-1.563 1.5354V6H10L9 9H6.2799v7h-3.257z" fill="currentColor"/></svg>
                  </a>
                  <!-- Twitter -->
                  <a href="http://twitter.com/share?url=<?php echo urlencode($url); ?>&text=<?php echo urlencode($title) . ' via @SportFloor'; ?>" onclick="window.open(this.href, 'twitterwindow','left=20,top=20,width=600,height=300,toolbar=0,resizable=1'); return false;">
                    <svg width="16" height="14" viewBox="0 0 16 14" fill="none"><path d="M16 2c-.6.3-1.2.4-1.9.5.7-.4 1.2-1 1.4-1.8-.6.4-1.3.6-2.1.8-.6-.6-1.5-1-2.4-1C9.3.5 7.8 2 7.8 3.8c0 .3 0 .5.1.7-2.7-.1-5.2-1.4-6.8-3.4-.3.5-.4 1-.4 1.7 0 1.1.6 2.1 1.5 2.7-.5 0-1-.2-1.5-.4C.7 6.7 1.8 8 3.3 8.3c-.3.1-.6.1-.9.1-.2 0-.4 0-.6-.1.4 1.3 1.6 2.3 3.1 2.3-1.1.9-2.5 1.4-4.1 1.4H0c1.5.9 3.2 1.5 5 1.5 6 0 9.3-5 9.3-9.3v-.4C15 3.3 15.6 2.7 16 2z" fill="currentColor"/></svg>
                  </a>
                  <!-- LinkedIn -->
                  <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode($url); ?>&title=<?php echo urlencode($title); ?>" onclick="window.open(this.href, 'linkedinwindow','left=20,top=20,width=600,height=700,toolbar=0,resizable=1'); return false;">
                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none"><g><path d="M15.3 0H.7C.3 0 0 .3 0 .7v14.7c0 .3.3.6.7.6h14.7c.4 0 .7-.3.7-.7V.7c-.1-.4-.4-.7-.8-.7zM4.7 13.6H2.4V6h2.4v7.6h-.1zM3.6 5c-.8 0-1.4-.7-1.4-1.4 0-.8.6-1.4 1.4-1.4.8 0 1.4.6 1.4 1.4-.1.7-.7 1.4-1.4 1.4zm10 8.6h-2.4V9.9c0-.9 0-2-1.2-2s-1.4 1-1.4 2v3.8H6.2V6h2.3v1c.3-.6 1.1-1.2 2.2-1.2 2.4 0 2.8 1.6 2.8 3.6v4.2h.1z" fill="currentColor"/></g><defs><clipPath><path fill="currentColor" d="M0 0h16v16H0z"/></clipPath></defs></svg>
                  </a>
                </div>
            </div>
        </div>
    </div>
<?php
    endwhile; // End of the loop.
?>
<?php
$categories = get_the_category($post->ID);
if ($categories)
{
    $category_ids = array();
    foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;

    $args=array(
        'category__in' => $category_ids,
        'post__not_in' => array($post->ID),
        'showposts'=>3,
        'caller_get_posts'=>1
    );
    $my_query = new wp_query($args);
    if( $my_query->have_posts() ) : ?>

        <section class="section section--gray">
            <div class="container">
                <div class="heading">
                    <h2 class="heading__title text--center">Related Articles</h2>
                </div>
                <div class="grid grid--three-columns grid--doubling">
                <?php
                while ($my_query->have_posts()) :
                    $my_query->the_post();
                    ?>
                    <div class="grid__column">
                        <?php echo get_template_part('template-parts/blog-grid'); ?>
                    </div>
                <?php endwhile; ?>
                </div>
            </div>
        </section>
    <?php
    endif;
    wp_reset_query();
}
?>

<?php
get_footer();
